/*
 * Launcher for Java applications. 
 * 
 * Based on PuppyGames Ultatron launcher: 
 * http://www.java-gaming.org/index.php?action=pastebin&hex=6f78e8e8d6a
 * 
 * Please note that the () source code to the Oracle Java launcher is also
 * available in the src.zip file contained in the JDK.
 */

#include <stdio.h>
#include <jni.h>
#include <sstream>
#include <string>
#include <windows.h>

#define NAME  			"Square 2"
#define MAIN_CLASS  	"nl/basvs/square/Square2"
#define CLASS_PATH      "square2.jar;lib/slick.jar;lib/lwjgl.jar"
#define JRE_PATH	  	"jre"

#define JVM_OPTIONS 	5

static JNIEnv *env;
static JavaVM *vm;

typedef jint (APIENTRY * CreateJavaVMPROC) (JavaVM **pvm, void **penv, void *args);

/*
 * Show an error message
 */
void messageBox(char * message, DWORD err) 
{
	std::ostringstream stream;
	stream << message << " error code: " << err;
	std::string str = stream.str();
	MessageBox(NULL, str.c_str(), NAME, MB_OK);
}

/*
 * Start the JVM and execute the game code.
 */
 int start(LPSTR lpCmdLine) 
{
	HANDLE hSingleInstanceMutex = CreateMutex(NULL, TRUE, NAME);
	DWORD dwError = GetLastError();
	if (dwError == ERROR_ALREADY_EXISTS)
	{
		// Application already lunched
		messageBox(NAME " is already running.", dwError);
		return 6;
	}
	
	SetEnvironmentVariable("_JAVA_OPTIONS", NULL);

	// JVM options. These were taken from the Ultatron launcher but may not be relevant for your application.
	JavaVMOption jvmOptions[JVM_OPTIONS];
	jvmOptions[0].optionString 	= "-Djava.class.path=" CLASS_PATH;
	jvmOptions[1].optionString 	= "-Xms256M";
	jvmOptions[2].optionString 	= "-Xmx512m";
	jvmOptions[3].optionString 	= "-XX:MaxGCPauseMillis=3";
	jvmOptions[4].optionString 	= "-Xincgc";

	SetLastError(0);
	
	HMODULE jvmdll = LoadLibrary(JRE_PATH "\\bin\\server\\jvm.dll");
	if (jvmdll == NULL) {
		messageBox("Failed to load Java DLL",  GetLastError());
		return 1;
	}
	
	JavaVMInitArgs vm_args;
	vm_args.version = JNI_VERSION_1_6;
	vm_args.options = jvmOptions;
	vm_args.nOptions = JVM_OPTIONS;
	vm_args.ignoreUnrecognized = FALSE;

	CreateJavaVMPROC CreateJavaVM = (CreateJavaVMPROC) GetProcAddress(jvmdll, "JNI_CreateJavaVM");
	
	// Create JVM
	jint res = CreateJavaVM(&vm, (void **)&env, &vm_args);
	if (res < 0) {
		messageBox("Failed to create Java virtual machine.",  GetLastError());
		return 2;
	}
	
	// Get the main class
	jclass cls = env->FindClass(MAIN_CLASS);
	if (cls == 0) {
		messageBox("Failed to find the main class.",  GetLastError());
		return 3;
	}
	
	// Get the method ID for the class's main(String[]) function.
	jmethodID mid = env->GetStaticMethodID(cls, "main", "([Ljava/lang/String;)V");
	if (mid == 0) {
		messageBox("Failed to find the main method.",  GetLastError());
		return 4;
	}
	
	// Pass a single string commandline arg
	jstring arguments = env->NewStringUTF(lpCmdLine == NULL ? "" : lpCmdLine);
	jobjectArray args = env->NewObjectArray(1, env->FindClass("java/lang/String"), NULL);
	env->SetObjectArrayElement(args, 0, arguments);
   
	// Run the main class...
	env->CallStaticVoidMethod(cls, mid, args);
	bool ok = true;
	if (env->ExceptionOccurred()) {
		ok = false;
		env->ExceptionDescribe();
	}
	vm->DestroyJavaVM();

	// Remove mutex; not strictly required as the OS cleans up after itself
	CloseHandle(hSingleInstanceMutex);

	return ok ? 0 : 5;
}

int main(const int argc, const char **argv) 
{
	return start((LPSTR) argv[1]);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	return start(lpCmdLine);
}