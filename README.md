# Square 2 #

I've been making prototypes and concept versions of games since I was a kid. Turning a prototype into a finished game, however, turned out to be a whole different challenge. Square 2 is my first attempt at creating a finished, polished game. 

It is a simple tetris clone with nice graphics and highscore lists, and was created in 2011 and written in Java using the Slick 2D game library.

![Square 2](https://bytebucket.org/Grunnt/square2/raw/996ce35af79e8e649be19f7845c0fd1a19538491/data/logobig.png)
