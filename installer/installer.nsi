# INCLUDES
!include MUI2.nsh ;Modern interface
!include LogicLib.nsh ;nsDialogs

# INIT
Name "Square2"
InstallDir "$LOCALAPPDATA\square2"
OutFile "Square2-Setup.exe"
RequestExecutionLevel user

# REFS
!define REG_UNINSTALL "Software\Microsoft\Windows\CurrentVersion\Uninstall\Square2"
!define START_LINK_DIR "$STARTMENU\Programs\Square2"
!define START_LINK_RUN "$STARTMENU\Programs\Square2\Square2.lnk"
!define START_LINK_UNINSTALLER "$STARTMENU\Programs\Square2\Uninstall Square2.lnk"
!define UNINSTALLER_NAME "Square2-Uninstall.exe"
!define WEBSITE_LINK "http://www.basvs.nl/square-ii/"
!define BUILD_DIR "..\builds\square2_b48\"

# GRAPHICS
!define MUI_ICON "installer.ico"
!define MUI_UNICON "uinstaller.ico"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_RIGHT
!define MUI_HEADERIMAGE_BITMAP "header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP "header.bmp"

# TEXT AND SETTINGS
!define MUI_PAGE_HEADER_TEXT "Square2"

!define MUI_FINISHPAGE_RUN "$INSTDIR\Square2.exe"
!define MUI_FINISHPAGE_RUN_TEXT "Run Square2 now."

;Do not skip to finish automatially
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

# PAGE DEFINITIONS
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# LANGUAGES
!insertmacro MUI_LANGUAGE "English"

# CALLBACKS
Function RegisterApplication
    ;Register uninstaller into Add/Remove panel (for local user only)
    WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayName" "Square2"
    WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayIcon" "$\"$INSTDIR\Square2.exe$\""
    WriteRegStr HKCU "${REG_UNINSTALL}" "Publisher" "BasvS"
    WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayVersion" "1.0"
    WriteRegDWord HKCU "${REG_UNINSTALL}" "EstimatedSize" 53000 ;KB
    WriteRegStr HKCU "${REG_UNINSTALL}" "HelpLink" "${WEBSITE_LINK}"
    WriteRegStr HKCU "${REG_UNINSTALL}" "URLInfoAbout" "${WEBSITE_LINK}"
    WriteRegStr HKCU "${REG_UNINSTALL}" "InstallLocation" "$\"$INSTDIR$\""
    WriteRegStr HKCU "${REG_UNINSTALL}" "InstallSource" "$\"$EXEDIR$\""
    WriteRegDWord HKCU "${REG_UNINSTALL}" "NoModify" 1
    WriteRegDWord HKCU "${REG_UNINSTALL}" "NoRepair" 1
    WriteRegStr HKCU "${REG_UNINSTALL}" "UninstallString" "$\"$INSTDIR\${UNINSTALLER_NAME}$\""
    WriteRegStr HKCU "${REG_UNINSTALL}" "Comments" "Uninstalls Square2."
    
    ;Links
    SetShellVarContext current
    CreateDirectory "${START_LINK_DIR}"
    CreateShortCut "${START_LINK_RUN}" "$INSTDIR\Square2.exe"
    CreateShortCut "${START_LINK_UNINSTALLER}" "$INSTDIR\${UNINSTALLER_NAME}"
FunctionEnd

Function un.DeregisterApplication
    ;Deregister uninstaller from Add/Remove panel
    DeleteRegKey HKCU "${REG_UNINSTALL}"
    
    ;Start menu links
    SetShellVarContext current
    RMDir /r "${START_LINK_DIR}"
FunctionEnd

# INSTALL SECTIONS
Section "!Square2" Square2
    SectionIn RO
    
    SetOutPath $INSTDIR
    SetOverwrite on
    
    ;Installation files are in selected build package
	File /r "${BUILD_DIR}\*.*"
        
    ;Uninstaller
    WriteUninstaller "$INSTDIR\${UNINSTALLER_NAME}"
    Call RegisterApplication
SectionEnd

Section "Uninstall"
    ;Remove whole directory (no data is stored there anyway)
    RMDir /r "$INSTDIR"
        
    ;Remove uninstaller
    Call un.DeregisterApplication
SectionEnd