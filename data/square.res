# Resource file list. 

# Gui resources. 
gui/button.png
gui/check.png
gui/checkbox.png
gui/panel.png
gui/slider.png
gui/slidermover.png
gui/textfield.png
gui/textfont.hiero
gui/cursor.png

# Game resources.
game/blocktiny.png
game/blocksmall.png
game/block.png
game/blocklarge.png
game/blockhuge.png
game/playfield.png

# Audio
audio/dot.wav
audio/drop.wav
audio/complete.wav
audio/bonus.wav
audio/action.wav

# Other resources
basvs.png
logobig.png
logosmall.png
loading.png
score.png
next.png
highest.png
gameover1.png
gameover2.png
square.png