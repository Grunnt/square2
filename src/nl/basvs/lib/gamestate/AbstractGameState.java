package nl.basvs.lib.gamestate;

import nl.basvs.lib.InputHelper;

import org.newdawn.slick.GameContainer;

/**
 * A base class for game states that handles transitions etcetera.
 */
public abstract class AbstractGameState {

	// Container and game
	protected GameContainer container;
	protected AbstractGame game;
	protected InputHelper input;

	// Enum for the status of this game state
	public enum State {
		Active, TransitionOn, Inactive, TransitionOff
	};

	// Exit the game when this state has transitioned off
	private boolean exitWhenInactive;

	// The time in ms it takes for this state to transition on and off
	private int transitionDuration;
	// Current progress in a transition
	private float transitionFactor;
	// Current state of this game state
	private State state;

	/**
	 * Check if the game should exit after this state has transitioned off.
	 * 
	 * @return
	 */
	public boolean isExitWhenInactive() {
		return exitWhenInactive;
	}

	/**
	 * Set whether the game should exit after this state has transitioned off.
	 * 
	 * @param exitWhenInactive
	 */
	public void setExitWhenInactive(boolean exitWhenInactive) {
		this.exitWhenInactive = exitWhenInactive;
	}

	/**
	 * Get the time in ms it takes for this state to transition on and off.
	 * 
	 * @return
	 */
	public int getTransitionDuration() {
		return transitionDuration;
	}

	/**
	 * Set the time in ms it takes for this state to transition on and off.
	 * 
	 * @param transitionDuration
	 */
	public void setTransitionDuration(int transitionDuration) {
		this.transitionDuration = transitionDuration;
	}

	/**
	 * Get the transition progress (1f = fully on, 0f = fully off)
	 * 
	 * @return
	 */
	public float getTransitionFactor() {
		return transitionFactor;
	}

	/**
	 * Check whether this state is currently transitioning on or off.
	 * 
	 * @return
	 */
	public boolean isTransitioning() {
		return (getState() == State.TransitionOn || getState() == State.TransitionOff);
	}

	/**
	 * Get this game states current state.
	 * 
	 * @return
	 */
	public State getState() {
		return state;
	}

	/**
	 * Set this element's status.
	 * 
	 * @param status
	 * @throws Exception
	 */
	public void setState(State state) throws Exception {
		// Prepare for a transition
		switch (state) {
		case Active:
			transitionFactor = 1f;
			this.state = State.Active;
			activating();
			break;
		case Inactive:
			transitionFactor = 0f;
			this.state = State.Inactive;
			deactivating();
			break;
		case TransitionOn:
			transitionFactor = 0f;
			if (this.state != State.Active)
				this.state = State.TransitionOn;
			activating();
			break;
		case TransitionOff:
			transitionFactor = 1f;
			if (this.state != State.Inactive)
				this.state = State.TransitionOff;
			deactivating();
			break;
		}
	}

	/**
	 * Create a new game state.
	 * 
	 * @param id
	 * @throws Exception
	 */
	public AbstractGameState(GameContainer container, AbstractGame game, InputHelper input) throws Exception {
		this.container = container;
		this.game = game;
		this.input = input;
		this.transitionDuration = 350;
		this.exitWhenInactive = false;
		// Start inactive
		setState(State.Inactive);
	}

	/**
	 * Render this game state.
	 * 
	 * @throws Exception
	 */
	public void render() throws Exception {
		// Only render when not inactive
		if (state != State.Inactive) {

			// Render the game state
			doRender();
		}
	}

	/**
	 * Code to render the game state.
	 */
	protected abstract void doRender() throws Exception;

	/**
	 * Update this game state.
	 * 
	 * @param delta
	 * @throws Exception
	 */
	public void update(int delta) throws Exception {

		// Only update when active
		if (state == State.Active)
			doUpdate(delta);

		// Update transitions
		if (state == State.TransitionOn) {
			float changeFactor = (float) delta / (float) transitionDuration;
			transitionFactor += changeFactor;
			if (transitionFactor >= 1f) {
				// Transition finished
				state = State.Active;
				transitionFactor = 1f;
				activated();
			}
		} else if (state == State.TransitionOff) {
			float changeFactor = (float) delta / (float) transitionDuration;
			transitionFactor -= changeFactor;
			if (transitionFactor <= 0f) {
				// Transition finished
				state = State.Inactive;
				transitionFactor = 0f;
				deactivated();

				// Exit the game if requested by the state
				if (exitWhenInactive)
					container.exit();
			}
		}
	}

	/**
	 * Code to update this game state.
	 * 
	 * @param delta
	 */
	protected abstract void doUpdate(int delta) throws Exception;

	/**
	 * Called before the state starts transitioning on.
	 */
	protected abstract void activating() throws Exception;

	/**
	 * Called before the state starts transitioning off.
	 */
	protected abstract void deactivating() throws Exception;

	/**
	 * Called after the state finished transitioning on.
	 */
	protected abstract void activated() throws Exception;

	/**
	 * Called after the state finished transitioning off.
	 */
	protected abstract void deactivated() throws Exception;
}
