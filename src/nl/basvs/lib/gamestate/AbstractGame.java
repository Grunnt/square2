package nl.basvs.lib.gamestate;

import java.util.HashMap;
import java.util.Stack;

import nl.basvs.lib.ErrorDialog;
import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gamestate.AbstractGameState.State;

import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 * A custom state based game class for Slick 2D.
 */
public abstract class AbstractGame implements Game {

	// Game container for easy access
	protected GameContainer container;
	// The list of game states, identified by a String id
	private HashMap<String, AbstractGameState> states = new HashMap<String, AbstractGameState>();
	// Two stacks for asynchronously activating and deactivating game states.
	// This will allow us to (de)activate a game
	// state after finishing the update loop of the states.
	private Stack<AbstractGameState> activateStack = new Stack<AbstractGameState>();
	private Stack<AbstractGameState> deactivateStack = new Stack<AbstractGameState>();
	// Title of this game
	private String title;
	// Input handler for easy access
	protected InputHelper input;

	/**
	 * Create a state-based game.
	 * 
	 * @param title
	 * @throws SlickException
	 */
	public AbstractGame(String title) throws SlickException {
		this.title = title;
	}

	/**
	 * Add a state to the game.
	 * 
	 * @param id
	 * @param state
	 */
	public void addState(String id, AbstractGameState state) {
		if (state == null) {
			throw new RuntimeException("State may not be null");
		}
		states.put(id, state);
	}

	/**
	 * Remove a state from the game.
	 * 
	 * @param id
	 */
	public void removeState(String id) {
		if (!states.containsKey(id)) {
			throw new RuntimeException("State does not exist");
		}
		states.remove(id);
	}

	/**
	 * Get a state.
	 * 
	 * @param id
	 * @return
	 */
	public AbstractGameState getState(String id) {
		if (!states.containsKey(id)) {
			throw new RuntimeException("State does not exist");
		}
		return states.get(id);
	}

	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * Activate a game state, and optionally deactivate the rest.
	 * 
	 * @param id
	 * @param deactivateRest
	 */
	public void activate(String id, boolean deactivateRest) {
		if (!states.containsKey(id)) {
			throw new RuntimeException("State does not exist");
		}
		if (deactivateRest) {
			for (String stateId : states.keySet()) {
				if (!stateId.equals(id)) {
					deactivate(stateId);
				}
			}
		}
		activateStack.push(states.get(id));
	}

	/**
	 * Deactivate a single panel.
	 * 
	 * @param id
	 */
	public void deactivate(String id) {
		if (!states.containsKey(id)) {
			throw new RuntimeException("State does not exist");
		}
		deactivateStack.push(states.get(id));
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		try {
			this.container = container;

			// Create the input helper wrapper
			input = new InputHelper(container.getInput());

			// Let the user do some initialization too
			initialize();
		} catch (Throwable t) {
			ErrorDialog.show(null, "Exception during init", t);
			System.exit(0);
		}
	}

	/**
	 * Add the required game states.
	 */
	protected abstract void initialize();

	@Override
	public void render(GameContainer container, Graphics graphics) throws SlickException {
		try {
			// Render all states first
			for (AbstractGameState state : states.values()) {
				state.render();
			}

		} catch (Throwable t) {
			ErrorDialog.show(null, "Exception during render", t);
			System.exit(0);
		}
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		try {
			if (container.hasFocus()) {
				// Update input status first of all
				input.update();

				// Update all states
				for (AbstractGameState state : states.values()) {
					state.update(delta);
				}

				// First check if any states are still transitioning on
				boolean anyTransitioningOn = false;
				for (AbstractGameState state : states.values()) {
					if (state.getState() == State.TransitionOn) {
						anyTransitioningOn = true;
						break;
					}
				}

				// Start transitioning off states if there are no states still
				// transitioning on
				if (!anyTransitioningOn) {
					while (!deactivateStack.isEmpty()) {
						deactivateStack.pop().setState(State.TransitionOff);
					}
				}

				// First check if any states are still transitioning off
				boolean anyTransitioningOff = false;
				for (AbstractGameState state : states.values()) {
					if (state.getState() == State.TransitionOff) {
						anyTransitioningOff = true;
						break;
					}
				}

				// Start transitioning states on if there are no states still
				// transitioning off
				if (!anyTransitioningOff) {
					while (!activateStack.isEmpty()) {
						activateStack.pop().setState(State.TransitionOn);
					}
				}
			}
		} catch (Throwable t) {
			ErrorDialog.show(null, "Exception during update", t);
			System.exit(0);
		}
	}

	@Override
	public boolean closeRequested() {
		return true;
	}
}