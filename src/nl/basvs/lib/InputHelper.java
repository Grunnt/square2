package nl.basvs.lib;

import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;

/**
 * A helper class which remembers an "old" input status from the last time it was updated, and provides extra
 * functionality such as getting the last key that was pressed.
 */
public class InputHelper implements KeyListener {

	private Input input;

	private boolean leftMouseDown;
	private boolean oldLeftMouseDown;

	private boolean middleMouseDown;
	private boolean oldMiddleMouseDown;

	private boolean rightMouseDown;
	private boolean oldRightMouseDown;

	private int mouseX;
	private int mouseY;

	private boolean keyPressed;
	private char lastCharacter;
	private int lastKey;

	/**
	 * Check if the left mouse button is down.
	 * 
	 * @return
	 */
	public boolean isLeftMouseDown() {
		return leftMouseDown;
	}

	/**
	 * Check if the middle mouse button is down.
	 * 
	 * @return
	 */
	public boolean isMiddleMouseDown() {
		return middleMouseDown;
	}

	/**
	 * Check if the right mouse button is down.
	 * 
	 * @return
	 */
	public boolean isRightMouseDown() {
		return rightMouseDown;
	}

	/**
	 * Check if the left mouse button was clicked (i.e. not down the last update and down now).
	 * 
	 * @return
	 */
	public boolean isLeftMouseClicked() {
		return (leftMouseDown && !oldLeftMouseDown);
	}

	/**
	 * Check if the middle mouse button was clicked (i.e. not down the last update and down now).
	 * 
	 * @return
	 */
	public boolean isMiddleMouseClicked() {
		return (middleMouseDown && !oldMiddleMouseDown);
	}

	/**
	 * Check if the right mouse button was clicked (i.e. not down the last update and down now).
	 * 
	 * @return
	 */
	public boolean isRightMouseClicked() {
		return (rightMouseDown && !oldRightMouseDown);
	}

	/**
	 * Get the mouse x coordinate.
	 * 
	 * @return
	 */
	public int getMouseX() {
		return mouseX;
	}

	/**
	 * Get the mouse y coordinate.
	 * 
	 * @return
	 */
	public int getMouseY() {
		return mouseY;
	}

	/**
	 * Create the input helper.
	 * 
	 * @param input
	 */
	public InputHelper(Input input) {
		this.input = input;
		// We add ourselves as keylistener so we can remember the last pressed key
		input.addKeyListener(this);
	}

	/**
	 * Update the input status, this will also remember the previous input status so we can detect changes.
	 */
	public void update() {
		// Remember the previous state so we can check for changes
		oldLeftMouseDown = leftMouseDown;
		oldMiddleMouseDown = middleMouseDown;
		oldRightMouseDown = rightMouseDown;

		// Update mouse button status
		leftMouseDown = input.isMouseButtonDown(0);
		middleMouseDown = input.isMouseButtonDown(1);
		rightMouseDown = input.isMouseButtonDown(2);

		// Update mouse coordinates
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();
	}

	/**
	 * Check if a key was pressed.
	 * 
	 * @return
	 */
	public boolean isKeyPressed() {
		return keyPressed;
	}

	/**
	 * Get the key that was pressed.
	 * 
	 * @return the key code, or -1 if no key was pressed
	 */
	public int getKeyPressed() {
		if (keyPressed)
			return lastKey;
		else
			return -1;
	}

	/**
	 * Check if the last pressed key was a printable ascii character.
	 * 
	 * @return
	 */
	public boolean isCharacterPressed() {
		if (keyPressed) {
			int charCode = (int) lastCharacter;
			if (charCode >= 32 && charCode <= 126)
				return true;
		}
		return false;
	}

	/**
	 * Get the last pressed character. If no character was pressed, return 0.
	 * 
	 * @return
	 */
	public char getCharacterPressed() {
		if (isCharacterPressed())
			return lastCharacter;
		else
			return 0;
	}

	/**
	 * Checks if a particular key is currently being pressed.
	 * 
	 * @param key
	 * @return
	 */
	public boolean isKeyDown(int key) {
		return input.isKeyDown(key);
	}

	@Override
	public void inputEnded() {
		// Do nothing
	}

	@Override
	public void inputStarted() {
		keyPressed = false;
	}

	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	@Override
	public void setInput(Input input) {
		// Do nothing
	}

	@Override
	public void keyPressed(int key, char c) {
		lastCharacter = c;
		lastKey = key;
		keyPressed = true;
	}

	@Override
	public void keyReleased(int key, char c) {
		// Do nothing
	}
}
