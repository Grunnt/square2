package nl.basvs.lib;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("serial")
public class ErrorDialog extends JDialog {

	public boolean busy = true;

	public ErrorDialog(String message, String details) {
		super();
		setSize(612, 412);
		setResizable(false);
		setTitle("Oops! Something went wrong...");
		setLayout(new BorderLayout());

		JLabel messageLabel = new JLabel(message);
		messageLabel.setFont(messageLabel.getFont().deriveFont(Font.BOLD, 15));
		messageLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(messageLabel, BorderLayout.NORTH);

		JTextArea detailTextArea = new JTextArea("Detais of the error:\n" + details);
		detailTextArea.setEditable(false);
		JScrollPane sp = new JScrollPane(detailTextArea);
		Dimension size = new Dimension(590, 350);
		sp.setPreferredSize(size);
		sp.setMinimumSize(size);
		sp.setMaximumSize(size);
		add(sp, BorderLayout.CENTER);

		JButton okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				busy = false;
				setVisible(false);
			}
		});
		add(okButton, BorderLayout.SOUTH);

		setLocationRelativeTo(null);
		pack();
		setVisible(true);
	}

	public static void show(JFrame frame, String message, Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		ErrorDialog ed = new ErrorDialog(message, sw.toString());
		System.out.println(sw.toString());
		while (ed.busy) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
		}
	}
}