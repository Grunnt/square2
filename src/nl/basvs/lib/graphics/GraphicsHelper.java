package nl.basvs.lib.graphics;

import org.newdawn.slick.Image;
import org.newdawn.slick.ImageBuffer;

public class GraphicsHelper {

	/**
	 * Generate an image.
	 * 
	 * @return
	 */
	public static Image generateImage(int w, int h, int r, int g, int b, int a) {
		ImageBuffer ib = new ImageBuffer(w, h);
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				ib.setRGBA(x, y, r, g, b, a);
			}
		}
		return ib.getImage();
	}

}
