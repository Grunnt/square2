package nl.basvs.lib.graphics;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;

/**
 * Helper class for drawing a (transparent) overlay that covers the display.
 */
public class Overlay {
	private Image image;
	private Color color;
	private GameContainer container;

	public Overlay(Color color, GameContainer container) {
		this.color = new Color(1f, 1f, 1f, 0f);
		this.container = container;
		image = GraphicsHelper.generateImage(2, 2, color.getRed(),
				color.getBlue(), color.getGreen(), color.getAlpha());
	}

	public void draw(float alpha) {
		color.a = alpha;
		image.draw(0, 0, container.getWidth(), container.getHeight(), color);
	}
}
