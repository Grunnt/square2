package nl.basvs.lib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

/**
 * A helper class for loading and storing application settings.
 */
public class SettingsHelper {

	private static SettingsHelper singleton = null;

	private String settingsFileName;
	private HashMap<String, Serializable> settings = null;

	/**
	 * Get the helper singleton instance. This will be null if initialize() has not been called first.
	 * 
	 * @return
	 */
	public static SettingsHelper getInstance() {
		return singleton;
	}

	/**
	 * Check if there are any settings.
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return settings.isEmpty();
	}

	/**
	 * Get a specific setting.
	 * 
	 * @param id
	 * @return
	 */
	public Serializable getSetting(String id) {
		return settings.get(id);
	}

	/**
	 * Set a specific setting to a new value.
	 * 
	 * @param id
	 * @param value
	 */
	public void putSetting(String id, Serializable value) {
		settings.put(id, value);
	}

	/**
	 * Create a new setting helper. This should only be called by initialize(), and not by other classes.
	 * 
	 * @param settingsFileName
	 */
	@SuppressWarnings("unchecked")
	private SettingsHelper(String settingsFileName) {
		this.settingsFileName = settingsFileName;

		try {
			// Read the HashMap from an object input stream
			FileInputStream fis = new FileInputStream(settingsFileName);
			ObjectInputStream ois = new ObjectInputStream(fis);

			settings = (HashMap<String, Serializable>) ois.readObject();

			ois.close();
		} catch (Exception e) {

			// Reading failed, so we return an empty set
			settings = new HashMap<String, Serializable>();
		}
	}

	/**
	 * Initialize the settings helper. This will try to load an existing settings file, and if this fails it will create
	 * an empty set (and NOT generate an exception, because the application should use default settings in this case).
	 * Check isEmpty() after initialization to check whether or not the helper succeeded in loading an existing settings
	 * file.
	 * 
	 * @param settingsFileName
	 * @return
	 */
	public static boolean initialize(String settingsFileName) {
		// Initialize the instance
		singleton = new SettingsHelper(settingsFileName);

		// Return false if there are no settings
		return !singleton.isEmpty();
	}

	/**
	 * Save the current settings to the settings file.
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		FileOutputStream fos = new FileOutputStream(settingsFileName);
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(settings);

		oos.close();
	}
}
