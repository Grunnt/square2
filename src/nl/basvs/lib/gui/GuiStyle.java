package nl.basvs.lib.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;

/**
 * A specific style, including images and colors, for the user interface.
 */
public class GuiStyle {
	private Color panelColor;
	private Color elementColor;
	private Color selectorColor;
	private Color highLightColor;
	private Color textColor;
	private Color titleColor;
	private UnicodeFont textFont;
	private UnicodeFont titleFont;
	private PartitionImage panelImage;
	private PartitionImage buttonImage;
	private PartitionImage buttonPressedImage;
	private PartitionImage textFieldImage;
	private PartitionImage sliderImage;
	private PartitionImage sliderMoverImage;
	private PartitionImage checkBoxImage;
	private Image checkBoxCheckedImage;
	private PartitionImage imageFrameImage;

	public Color getPanelColor() {
		return panelColor;
	}

	public void setPanelColor(Color panelColor) {
		this.panelColor = panelColor;
	}

	public Color getElementColor() {
		return elementColor;
	}

	public void setElementColor(Color elementColor) {
		this.elementColor = elementColor;
	}

	public Color getSelectorColor() {
		return selectorColor;
	}

	public void setSelectorColor(Color selectorColor) {
		this.selectorColor = selectorColor;
	}

	public Color getHighLightColor() {
		return highLightColor;
	}

	public void setHighLightColor(Color highLightColor) {
		this.highLightColor = highLightColor;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	public Color getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(Color titleColor) {
		this.titleColor = titleColor;
	}

	public UnicodeFont getTextFont() {
		return textFont;
	}

	public void setTextFont(UnicodeFont textFont) {
		this.textFont = textFont;
	}

	public UnicodeFont getTitleFont() {
		return titleFont;
	}

	public void setTitleFont(UnicodeFont titleFont) {
		this.titleFont = titleFont;
	}

	public PartitionImage getPanelImage() {
		return panelImage;
	}

	public void setPanelImage(PartitionImage panelImage) {
		this.panelImage = panelImage;
	}

	public PartitionImage getButtonImage() {
		return buttonImage;
	}

	public void setButtonImage(PartitionImage buttonImage) {
		this.buttonImage = buttonImage;
	}

	public PartitionImage getButtonPressedImage() {
		return buttonPressedImage;
	}

	public void setButtonPressedImage(PartitionImage buttonPressedImage) {
		this.buttonPressedImage = buttonPressedImage;
	}

	public PartitionImage getTextFieldImage() {
		return textFieldImage;
	}

	public void setTextFieldImage(PartitionImage textFieldImage) {
		this.textFieldImage = textFieldImage;
	}

	public PartitionImage getSliderImage() {
		return sliderImage;
	}

	public void setSliderImage(PartitionImage sliderImage) {
		this.sliderImage = sliderImage;
	}

	public PartitionImage getSliderMoverImage() {
		return sliderMoverImage;
	}

	public void setSliderMoverImage(PartitionImage sliderMoverImage) {
		this.sliderMoverImage = sliderMoverImage;
	}

	public PartitionImage getCheckBoxImage() {
		return checkBoxImage;
	}

	public void setCheckBoxImage(PartitionImage checkBoxImage) {
		this.checkBoxImage = checkBoxImage;
	}

	public Image getCheckBoxCheckedImage() {
		return checkBoxCheckedImage;
	}

	public void setCheckBoxCheckedImage(Image checkBoxCheckedImage) {
		this.checkBoxCheckedImage = checkBoxCheckedImage;
	}

	public PartitionImage getImageFrameImage() {
		return imageFrameImage;
	}

	public void setImageFrameImage(PartitionImage imageFrameImage) {
		this.imageFrameImage = imageFrameImage;
	}
}
