package nl.basvs.lib.gui;

import org.newdawn.slick.Image;

public class GuiCursor {
	private Image cursorImage;
	private int hotspotX, hotspotY;

	public GuiCursor(Image cursorImage, int hotspotX, int hotspotY) {
		this.cursorImage = cursorImage;
		this.hotspotX = hotspotX;
		this.hotspotY = hotspotY;
	}

	public void render(int x, int y) {
		cursorImage.draw(x - hotspotX, y - hotspotY);
	}
}
