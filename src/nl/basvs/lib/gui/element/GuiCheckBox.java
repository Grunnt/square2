package nl.basvs.lib.gui.element;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;

import org.newdawn.slick.Color;

/**
 * A checkbox Gui element with a title.
 */
public class GuiCheckBox extends AbstractGuiElement {

	protected String title;
	protected boolean checked;
	protected InputHelper input;

	/**
	 * Get the checkbox title.
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the checkbox title.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Check if the checkbox is checked. :-)
	 * 
	 * @return
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * Change the checked status.
	 * 
	 * @param checked
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * Create a new checkbox.
	 * 
	 * @param input
	 * @param id
	 * @param title
	 * @param checked
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public GuiCheckBox(InputHelper input, String id, String title, boolean checked, int x, int y, int w, int h,
			int margin, GuiStyle style) {
		super(id, x, y, w, h, margin, style);
		this.input = input;
		this.title = title;
		this.checked = checked;
	}

	@Override
	protected void doRender() {
		// Draw the button
		Color drawColor = style.getElementColor();
		if (isInBounds(input.getMouseX(), input.getMouseY()))
			drawColor = style.getHighLightColor();
		style.getButtonImage().draw(getX(), getY(), getWidth(), getHeight(), drawColor);

		// Draw the checkbox
		style.getCheckBoxImage().draw(getContentX() + getContentW() - getContentH(), getContentY(), getContentH(),
				getContentH(), drawColor);
		if (checked) {
			style.getCheckBoxCheckedImage().draw(getContentX() + getContentW() - getContentH(), getContentY(),
					getContentH(), getContentH(), style.getSelectorColor());
		}

		// Draw the title
		int xTitle = getCenterX() - ((style.getTextFont().getWidth(getTitle()) + getContentH() + margin) / 2);
		int yTitle = getCenterY() - (style.getTextFont().getLineHeight() / 2);
		style.getTextFont().drawString(xTitle, yTitle, getTitle(), style.getTextColor());
	};

	@Override
	protected void doUpdate(int delta) throws Exception {
		if (input.isLeftMouseClicked()) {
			if (isInBounds(input.getMouseX(), input.getMouseY())) {
				// The checkbox was clicked, so invert its checked status
				checked = !checked;
				for (IGuiListener listener : listeners) {
					listener.action(this);
				}
			}
		}
	}

	@Override
	public void preActivation() {
	}
}
