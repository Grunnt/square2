package nl.basvs.lib.gui.element;

import nl.basvs.lib.gui.GuiStyle;

import org.newdawn.slick.Image;

/**
 * A simple Gui element for showing an image.
 */
public class GuiImage extends AbstractGuiElement {

	private Image image;

	/**
	 * Get the image that is shown.
	 * 
	 * @return
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * Set the image that is shown.
	 * 
	 * @param image
	 */
	public void setImage(Image image) {
		this.image = image;
	}

	/**
	 * Create the gui element.
	 * 
	 * @param id
	 * @param image
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public GuiImage(String id, Image image, int x, int y, int w, int h, int margin, GuiStyle style) {
		super(id, x, y, w, h, margin, style);
		this.image = image;
	}

	@Override
	protected void doRender() {
		// Draw the image
		if (image != null)
			image.draw(getContentX(), getContentY(), getContentW(), getContentH());

		// Draw the frame over it
		style.getImageFrameImage().draw(getX(), getY(), getWidth(), getHeight(), style.getElementColor());
	}

	@Override
	protected void doUpdate(int delta) {
		// Do nothing
	}

	@Override
	public void preActivation() {
	}
}
