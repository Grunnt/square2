package nl.basvs.lib.gui.element;

import java.util.ArrayList;
import java.util.List;

import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;
import nl.basvs.lib.gui.transition.AbstractGuiTransitionEffect;

/**
 * Most basic GuiElement.
 */
/**
 * @author Bas
 * 
 */
public abstract class AbstractGuiElement {

	/**
	 * Status of the GuiElement.
	 */
	public enum GuiStatus {
		Active, TransitionOn, Inactive, TransitionOff
	};

	protected GuiStatus status;
	protected float transitionFactor;
	protected int transitionDuration;
	protected AbstractGuiTransitionEffect transitionEffect;

	protected int x, y, w, h, margin;
	protected GuiStyle style;
	protected List<IGuiListener> listeners = new ArrayList<IGuiListener>();

	protected String id;

	/**
	 * Get this element's position (relative to its parent).
	 * 
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * Set this element's position (relative to its parent).
	 * 
	 * @return
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Get this element's position (relative to its parent).
	 * 
	 * @return
	 */
	public int getY() {
		return y;
	}

	/**
	 * Set this element's position (relative to its parent).
	 * 
	 * @return
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Get this element's width.
	 * 
	 * @return
	 */
	public int getWidth() {
		return w;
	}

	/**
	 * Set this element's width.
	 * 
	 * @return
	 */
	public void setWidth(int w) {
		this.w = w;
	}

	/**
	 * Get this element's height.
	 * 
	 * @return
	 */
	public int getHeight() {
		return h;
	}

	/**
	 * Set this element's height.
	 * 
	 * @return
	 */
	public void setHeight(int h) {
		this.h = h;
	}

	/**
	 * Set the margin between the outer bounds and content area of this element.
	 * 
	 * @return
	 */
	public int getMargin() {
		return margin;
	}

	/**
	 * Set the margin between the outer bounds and content area of this element.
	 * 
	 * @param margin
	 */
	public void setMargin(int margin) {
		this.margin = margin;
	}

	/**
	 * Get the style used to render this element.
	 * 
	 * @return
	 */
	public GuiStyle getStyle() {
		return style;
	}

	/**
	 * Set the style used to render this element.
	 * 
	 * @param style
	 */
	public void setStyle(GuiStyle style) {
		this.style = style;
	}

	/**
	 * Get this element's status.
	 * 
	 * @return
	 */
	public GuiStatus getStatus() {
		return status;
	}

	/**
	 * Set this element's status.
	 * 
	 * @param status
	 */
	public void setStatus(GuiStatus status) {
		// Prepare for a transition
		switch (status) {
		case Active:
			transitionFactor = 1f;
			this.status = GuiStatus.Active;
			break;
		case Inactive:
			transitionFactor = 0f;
			this.status = GuiStatus.Inactive;
			break;
		case TransitionOn:
			transitionFactor = 0f;
			if (this.status != GuiStatus.Active)
				this.status = GuiStatus.TransitionOn;
			break;
		case TransitionOff:
			transitionFactor = 1f;
			if (this.status != GuiStatus.Inactive)
				this.status = GuiStatus.TransitionOff;
			break;
		}
	}

	/**
	 * Get how far along a transition this element is.
	 * 
	 * @return transition progress ranging from 0f (transitioned off, inactive) to 1f (transitioned on, active).
	 */
	public float getTransitionFactor() {
		return transitionFactor;
	}

	/**
	 * Get the time it will take this element to transition on or off.
	 * 
	 * @return
	 */
	public int getTransitionDuration() {
		return transitionDuration;
	}

	/**
	 * Set the time it will take this element to transition on or off.
	 * 
	 * @param transitionDuration
	 */
	public void setTransitionDuration(int transitionDuration) {
		this.transitionDuration = transitionDuration;
	}

	/**
	 * Get the effect that will be used for visual transition effects.
	 * 
	 * @return
	 */
	public AbstractGuiTransitionEffect getTransitionEffect() {
		return transitionEffect;
	}

	/**
	 * Set the effect that will be used for visual transition effects.
	 * 
	 * @param transitionEffect
	 */
	public void setTransitionEffect(AbstractGuiTransitionEffect transitionEffect) {
		this.transitionEffect = transitionEffect;
	}

	/**
	 * Add a listener that receives events from this element.
	 * 
	 * @param listener
	 */
	public void addListener(IGuiListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove a listener.
	 * 
	 * @param listener
	 */
	public void removeListener(IGuiListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Remove all listeners.
	 */
	public void removeAllListeners() {
		listeners.clear();
	}

	/**
	 * Get this element's id (useful for identifying interactive elements).
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Create a new GUI element.
	 * 
	 * @param id
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public AbstractGuiElement(String id, int x, int y, int w, int h, int margin, GuiStyle style) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.margin = margin;
		this.style = style;
		// Elements start active by default.
		setStatus(GuiStatus.Active);
		// Set default transition duration in milliseconds.
		this.transitionDuration = 350;
		// No transition by default
		this.transitionEffect = null;
	}

	/**
	 * Render this element.
	 * 
	 * @throws Exception
	 */
	public final void render() throws Exception {
		// Only render not inactive
		if (status != GuiStatus.Inactive) {
			if (transitionEffect != null && (status == GuiStatus.TransitionOn || status == GuiStatus.TransitionOff)) {
				// Setup graphics transform for a transition effect
				transitionEffect.setupEffect(transitionFactor);
			}
			doRender();
			if (transitionEffect != null && (status == GuiStatus.TransitionOn || status == GuiStatus.TransitionOff)) {
				// Reset graphics transform.
				transitionEffect.clearEffect();
			}
		}
	}

	protected abstract void doRender() throws Exception;

	/**
	 * Update this element.
	 * 
	 * @param delta
	 * @throws Exception
	 */
	public final void update(int delta) throws Exception {
		// Only update (i.e. handle input) when active
		if (status == GuiStatus.Active)
			doUpdate(delta);

		if (status == GuiStatus.TransitionOn) {
			float changeFactor = (float) delta / (float) transitionDuration;
			transitionFactor += changeFactor;
			if (transitionFactor >= 1f) {
				// Transition finished
				setStatus(GuiStatus.Active);
				transitionFactor = 1f;
			}
		} else if (status == GuiStatus.TransitionOff) {
			float changeFactor = (float) delta / (float) transitionDuration;
			transitionFactor -= changeFactor;
			if (transitionFactor <= 0f) {
				// Transition finished
				setStatus(GuiStatus.Inactive);
				transitionFactor = 0f;
			}
		}
	}

	/**
	 * Handle updates to the element.
	 * 
	 * @param delta
	 * @throws Exception
	 */
	protected abstract void doUpdate(int delta) throws Exception;

	/**
	 * Check if a point is inside the element's outer bounds.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isInBounds(int x, int y) {
		if (x > this.x && x < this.x + w && y > this.y && y < this.y + h)
			return true;
		else
			return false;
	}

	/**
	 * Get the absolute center Y.
	 * 
	 * @return
	 */
	public int getCenterX() {
		return x + w / 2;
	}

	/**
	 * Get the absolute center Y.
	 * 
	 * @return
	 */
	public int getCenterY() {
		return y + h / 2;
	}

	/**
	 * Get the x of the content area (which is its outer bonds minus margin) of this element.
	 * 
	 * @return
	 */
	public int getContentX() {
		return x + margin;
	}

	/**
	 * Get the y of the content area (which is its outer bonds minus margin) of this element.
	 * 
	 * @return
	 */
	public int getContentY() {
		return y + margin;
	}

	/**
	 * Get the width of the content area (which is its outer size minus 2 * margin) of this element.
	 * 
	 * @return
	 */
	public int getContentW() {
		return w - 2 * margin;
	}

	/**
	 * Get the height of the content area (which is its outer size minus 2 * margin) of this element.
	 * 
	 * @return
	 */
	public int getContentH() {
		return h - 2 * margin;
	}

	/**
	 * Called before the element transitions on.
	 */
	public abstract void preActivation();
}
