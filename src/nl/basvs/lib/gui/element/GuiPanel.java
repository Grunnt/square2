package nl.basvs.lib.gui.element;

import java.util.ArrayList;
import java.util.List;

import nl.basvs.lib.gui.GuiStyle;

/**
 * A panel element which may contain other gui elements. It has a background.
 */
public class GuiPanel extends AbstractGuiElement {

	protected List<AbstractGuiElement> children = new ArrayList<AbstractGuiElement>();

	/**
	 * Get all children elements.
	 * 
	 * @return
	 */
	public List<AbstractGuiElement> getChildren() {
		return children;
	}

	/**
	 * Add a child element.
	 * 
	 * @param child
	 */
	public void addChild(AbstractGuiElement child) {
		children.add(child);
	}

	/**
	 * Remove a child element.
	 * 
	 * @param child
	 */
	public void removeChild(AbstractGuiElement child) {
		children.remove(child);
	}

	/**
	 * Remove all child elements.
	 */
	public void removeAllChildren() {
		children.clear();
	}

	/**
	 * Create a new panel.
	 * 
	 * @param id
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public GuiPanel(String id, int x, int y, int w, int h, int margin, GuiStyle style) {
		super(id, x, y, w, h, margin, style);
	}

	@Override
	protected void doRender() throws Exception {
		style.getPanelImage().draw(getX(), getY(), getWidth(), getHeight(), style.getPanelColor());
		for (AbstractGuiElement child : children) {
			child.render();
		}
	}

	@Override
	protected void doUpdate(int delta) throws Exception {
		for (AbstractGuiElement child : children) {
			child.update(delta);
		}
	}

	@Override
	public void preActivation() {
	}
}
