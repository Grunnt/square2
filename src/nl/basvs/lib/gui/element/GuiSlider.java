package nl.basvs.lib.gui.element;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;

import org.newdawn.slick.Color;

/**
 * A slider element with a title.
 */
public class GuiSlider extends AbstractGuiElement {

	protected final static int SLIDER_MOVER_WIDTH_FACTOR = 10;

	protected String title;
	protected float value;
	protected boolean stoppedDragging;
	protected boolean wasDragging;
	protected InputHelper input;

	/**
	 * Get the title of the slider.
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title of the slider.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get the value of this slider, which is between 0f (all the way to the left) and 1f (all the way to the right),
	 * inclusive.
	 * 
	 * @return
	 */
	public float getValue() {
		return value;
	}

	/**
	 * Set the value of this slider, which is between 0f (all the way to the left) and 1f (all the way to the right),
	 * inclusive.
	 * 
	 * @param value
	 */
	public void setValue(float value) {
		this.value = value;
		if (this.value < 0f)
			this.value = 0f;
		else if (this.value > 1f)
			this.value = 1f;
	}

	/**
	 * Check if the user stopped dragging this slider in the last update.
	 * 
	 * @return
	 */
	public boolean isStoppedDragging() {
		return stoppedDragging;
	}

	public GuiSlider(InputHelper input, String id, String title, float value, int x, int y, int w, int h, int margin,
			GuiStyle style) {
		super(id, x, y, w, h, margin, style);
		this.input = input;
		this.title = title;
		this.value = value;
	}

	@Override
	protected void doRender() {
		// Draw the button background
		Color drawColor = style.getElementColor();
		if (isInBounds(input.getMouseX(), input.getMouseY()))
			drawColor = style.getHighLightColor();
		style.getButtonImage().draw(getX(), getY(), getWidth(), getHeight(), drawColor);

		// Draw the title
		int textWidth = style.getTextFont().getWidth(getTitle()) + 2 * margin;
		int xTitle = getContentX() + margin;
		int yTitle = getCenterY() - (style.getTextFont().getLineHeight() / 2);
		style.getTextFont().drawString(xTitle, yTitle, getTitle(), style.getTextColor());

		// Draw the slider
		int sliderWidth = getContentW() - textWidth;
		style.getSliderImage().draw(getContentX() + textWidth, getContentY(), sliderWidth, getContentH(), drawColor);

		// Draw the slider mover
		int sliderMoverX = (int) (getContentX() + textWidth + style.getSliderImage().getPartition() + (value * (sliderWidth - style
				.getSliderImage().getPartition() * 2)));
		int sliderMoverW = sliderWidth / SLIDER_MOVER_WIDTH_FACTOR;
		style.getSliderMoverImage().draw(sliderMoverX - sliderMoverW / 2, getContentY() + margin, sliderMoverW,
				getContentH() - 2 * margin, style.getSelectorColor());
	};

	@Override
	protected void doUpdate(int delta) throws Exception {
		boolean draggingNow = false;

		if (input.isLeftMouseDown()) {
			if (isInBounds(input.getMouseX(), input.getMouseY())) {
				// Adjust value of the slider, since the slider was clicked
				int textWidth = style.getTextFont().getWidth(getTitle()) + 2 * margin;
				int sliderWidth = getContentW() - (textWidth + 2 * margin);
				int sliderX = getContentX() + textWidth + margin;

				int relativeX = input.getMouseX() - sliderX;
				value = (float) relativeX / (float) sliderWidth;
				if (value > 1f)
					value = 1f;
				else if (value < 0f)
					value = 0f;

				// And signal listeners that the value has changed
				for (IGuiListener listener : listeners) {
					listener.action(this);
				}

				// The slider is being dragged (i.e. mouse is being held down on it).
				draggingNow = true;
			}
		}

		// It's useful to remember whether or not the user stopped dragging the slider
		if (wasDragging && !draggingNow) {
			stoppedDragging = true;
		} else {
			stoppedDragging = false;
		}
		wasDragging = draggingNow;
	}

	@Override
	public void preActivation() {
	}
}