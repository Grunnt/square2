package nl.basvs.lib.gui.element;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;

import org.newdawn.slick.Color;
import org.newdawn.slick.Input;

/**
 * A text field gui element which will receive text input if the mouse cursor is over it.
 */
public class GuiTextField extends AbstractGuiElement {

	protected String title;
	protected String text;
	protected int maxTextLength;
	protected InputHelper input;
	protected boolean focus;

	/**
	 * Get the title of the element.
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title of the element.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Set the text in the editable field.
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set the text in the editable field.
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Get the maximum number of characters allowed in the text field.
	 * 
	 * @return
	 */
	public int getMaxTextLength() {
		return maxTextLength;
	}

	/**
	 * Set the maximum number of characters allowed in the text field.
	 * 
	 * @param maxTextLength
	 */
	public void setMaxTextLength(int maxTextLength) {
		this.maxTextLength = maxTextLength;
	}

	/**
	 * Check whether this text field has focus, which means whether it should respond to text input.
	 * 
	 * @return
	 */
	public boolean hasFocus() {
		return focus;
	}

	/**
	 * Set whether this text field has focus, which means whether it should respond to text input.
	 * 
	 * @param focus
	 */
	public void setFocus(boolean focus) {
		this.focus = focus;
	}

	/**
	 * Create a new text field element (by default has max text length of 10).
	 * 
	 * @param input
	 * @param id
	 * @param title
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public GuiTextField(InputHelper input, String id, String title, int x, int y, int w, int h, int margin,
			GuiStyle style) {
		super(id, x, y, w, h, margin, style);
		this.input = input;
		this.title = title;
		this.maxTextLength = 10;
		text = "";
	}

	@Override
	protected void doRender() {
		// Draw the button background
		Color drawColor = style.getElementColor();
		if (isInBounds(input.getMouseX(), input.getMouseY()))
			drawColor = style.getHighLightColor();
		style.getButtonImage().draw(getX(), getY(), getWidth(), getHeight(), drawColor);

		// Draw the title
		int titleWidth = style.getTextFont().getWidth(getTitle()) + 2 * margin;
		int xTitle = getContentX() + margin;
		int yTitle = getCenterY() - (style.getTextFont().getLineHeight() / 2);
		style.getTextFont().drawString(xTitle, yTitle, getTitle(), style.getTextColor());

		// Draw the text field area
		int fieldWidth = getContentW() - titleWidth;
		style.getTextFieldImage().draw(getContentX() + titleWidth, getContentY(), fieldWidth, getContentH(), drawColor);

		// Draw the text
		style.getTextFont().drawString(getContentX() + titleWidth + margin, yTitle, getText(), style.getTextColor());
	};

	@Override
	protected void doUpdate(int delta) throws Exception {
		if (isInBounds(input.getMouseX(), input.getMouseY()) || hasFocus()) {
			boolean entryComplete = false;
			if (input.isKeyPressed()) {
				if (input.isCharacterPressed()) {
					// Add a character
					if (text.length() < maxTextLength)
						text = text + input.getCharacterPressed();
				} else if (input.getKeyPressed() == Input.KEY_BACK) {
					// Remove the last character
					if (text.length() > 0)
						text = text.substring(0, text.length() - 1);
				} else if (input.getKeyPressed() == Input.KEY_ENTER) {
					entryComplete = true;
				}
			}

			if (entryComplete) {
				// Text entry complete
				for (IGuiListener listener : listeners) {
					listener.action(this);
				}
			}
		}
	}

	@Override
	public void preActivation() {
	}
}