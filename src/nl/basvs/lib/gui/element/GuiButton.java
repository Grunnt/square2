package nl.basvs.lib.gui.element;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;

import org.newdawn.slick.Color;

/**
 * A simple clickable button with a title.
 */
public class GuiButton extends AbstractGuiElement {

	protected String title;
	protected InputHelper input;

	/**
	 * Get the title of this button.
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title of this button.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Create a new button. Clicking the button will generate an action event for its listeners.
	 * 
	 * @param input
	 * @param id
	 * @param title
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param margin
	 * @param style
	 */
	public GuiButton(InputHelper input, String id, String title, int x, int y, int w, int h, int margin, GuiStyle style) {
		super(id, x, y, w, h, margin, style);
		this.input = input;
		this.title = title;
	}

	@Override
	protected void doRender() {
		// Draw the button
		Color drawColor = style.getElementColor();
		if (isInBounds(input.getMouseX(), input.getMouseY()))
			drawColor = style.getHighLightColor();
		style.getButtonImage().draw(getX(), getY(), getWidth(), getHeight(), drawColor);

		// Draw the title on it
		int xTitle = getCenterX() - (style.getTextFont().getWidth(getTitle()) / 2);
		int yTitle = getCenterY() - (style.getTextFont().getLineHeight() / 2);
		style.getTextFont().drawString(xTitle, yTitle, getTitle(), style.getTextColor());
	};

	@Override
	protected void doUpdate(int delta) throws Exception {
		if (input.isLeftMouseClicked()) {
			if (isInBounds(input.getMouseX(), input.getMouseY())) {
				for (IGuiListener listener : listeners) {
					listener.action(this);
				}
			}
		}
	}

	@Override
	public void preActivation() {
	}
}
