package nl.basvs.lib.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;

/**
 * A partitioned image which is partitioned in 9 parts (4 corners, 4 sides and a center) to avoid distorting the corners
 * when stretching the image.
 */
public class PartitionImage {
	private Image image;
	private int partition;

	private int getX0() {
		return 0;
	}

	private int getX1() {
		return partition;
	}

	private int getX2() {
		return image.getWidth() - partition;
	}

	private int getX3() {
		return image.getWidth();
	}

	private int getY0() {
		return 0;
	}

	private int getY1() {
		return partition;
	}

	private int getY2() {
		return image.getHeight() - partition;
	}

	private int getY3() {
		return image.getHeight();
	}

	public int getPartition() {
		return partition;
	}

	/**
	 * Create a partitioned image which is partitioned in 9 parts (4 corners, 4 sides and a center) to avoid distorting
	 * the corners when stretching the image.
	 * 
	 * @param image
	 * @param partition
	 */
	public PartitionImage(Image image, int partition) {
		this.image = image;
		this.partition = partition;
	}

	/**
	 * Draw the partitioned image with the indicated size. The corner partitions are drawn in the original size, and the
	 * sides and center are stretched to fit.
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param color
	 */
	public void draw(int x, int y, int w, int h, Color color) {

		int realPartitionX = partition;
		int realPartitionY = partition;

		// In case the size is smaller than the image, we also need to scale the corners.
		if (w < partition * 2) {
			realPartitionX = w / 2;
		}

		// In case the size is smaller than the image, we also need to scale the corners.
		if (h < partition * 2) {
			realPartitionY = h / 2;
		}

		// Determine the destination rectangles
		int dx0 = x;
		int dx1 = x + realPartitionX;
		int dx2 = x + w - realPartitionX;
		int dx3 = x + w;
		int dy0 = y;
		int dy1 = y + realPartitionY;
		int dy2 = y + h - realPartitionY;
		int dy3 = y + h;

		// Draw center
		image.draw(dx1, dy1, dx2, dy2, getX1(), getY1(), getX2(), getY2(), color);

		// Draw top edge
		image.draw(dx1, dy0, dx2, dy1, getX1(), getY0(), getX2(), getY1(), color);

		// Draw bottom edge
		image.draw(dx1, dy2, dx2, dy3, getX1(), getY2(), getX2(), getY3(), color);
		// Draw left edge
		image.draw(dx0, dy1, dx1, dy2, getX0(), getY1(), getX1(), getY2(), color);
		// Draw right edge
		image.draw(dx2, dy1, dx3, dy2, getX2(), getY1(), getX3(), getY2(), color);

		// Draw top-left corner
		image.draw(dx0, dy0, dx1, dy1, getX0(), getY0(), getX1(), getY1(), color);
		// Draw top-right corner
		image.draw(dx2, dy0, dx3, dy1, getX2(), getY0(), getX3(), getY1(), color);
		// Draw bottom-right corner
		image.draw(dx2, dy2, dx3, dy3, getX2(), getY2(), getX3(), getY3(), color);
		// Draw bottom-left corner
		image.draw(dx0, dy2, dx1, dy3, getX0(), getY2(), getX1(), getY3(), color);
	}
}
