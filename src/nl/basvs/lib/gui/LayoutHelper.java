package nl.basvs.lib.gui;

import java.util.List;

import nl.basvs.lib.gui.element.AbstractGuiElement;
import nl.basvs.lib.gui.element.GuiPanel;

/**
 * A helper class for doing GuiPanel layout more easily.
 */
public class LayoutHelper {

	public enum VerticalAlignment {
		Top, Centered, Bottom, Fill
	};

	public enum HorizontalAlignment {
		Left, Centered, Right, Fill
	};

	/**
	 * Layout all elements in a panel, including its subpanels.
	 * 
	 * @param panel
	 * @param xAlign
	 * @param xPadding
	 * @param yAlign
	 * @param yPadding
	 * @param resizeParent
	 * @param columns
	 */
	public static void layoutRows(GuiPanel panel, HorizontalAlignment xAlign, int xPadding, VerticalAlignment yAlign,
			int yPadding, boolean resizeParent, int... columns) {

		// First position all elements relative to the 0,0 point
		List<AbstractGuiElement> e = panel.getChildren();
		int n = 0;
		int row = 0;
		int totalHeight = 0;
		int totalWidth = 0;
		int currentY = 0;
		while (n < e.size()) {
			int columnsThisRow = 1; // 1 column by default
			if (row < columns.length)
				columnsThisRow = columns[row];

			// Calculate one row dimensions
			int rowHeight = 0;
			int rowWidth = 0;
			for (int c = 0; c < columnsThisRow && c + n < e.size(); c++) {
				// Get the max height
				if (e.get(n + c).getHeight() > rowHeight)
					rowHeight = e.get(n + c).getHeight();
				// Add up the column widths
				rowWidth += e.get(n + c).getWidth();
			}
			// Add the padding
			rowWidth += xPadding * 2 * columnsThisRow;

			if (rowWidth > totalWidth)
				totalWidth = rowWidth;

			// Add up to the total height
			totalHeight += rowHeight + 2 * yPadding;

			// Determine horizontal starting point based on alignment
			int currentX = 0; // Left align by default
			if (xAlign == HorizontalAlignment.Centered) {
				// Start so things are centered
				currentX = panel.getContentW() / 2 - rowWidth / 2;
			} else if (xAlign == HorizontalAlignment.Right) {
				// Start so things end up aligned to the right side
				currentX = panel.getContentW() - rowWidth;
			}

			// Now position the row's elements
			currentY += yPadding;
			for (int c = 0; c < columnsThisRow && c + n < e.size(); c++) {
				// Position horizontally
				currentX += xPadding;
				AbstractGuiElement el = e.get(n + c);
				el.setX(currentX);
				currentX += el.getWidth() + xPadding;
				// Position vertically
				el.setY(currentY);
			}
			currentY += rowHeight + yPadding;

			n += columnsThisRow;
			row++;
		}

		// Now translate to the correct position
		int translateX = panel.getContentX();
		int translateY = panel.getContentY();
		if (yAlign == VerticalAlignment.Centered) {
			translateY = panel.getCenterY() - totalHeight / 2;
		} else if (yAlign == VerticalAlignment.Bottom) {
			translateY = panel.getContentY() + panel.getContentH() - totalHeight;
		}
		for (AbstractGuiElement el : panel.getChildren()) {
			el.setX(el.getX() + translateX);
			el.setY(el.getY() + translateY);
		}

		// Resize the parent to nicely fit around the contents.
		if (resizeParent) {
			int oldCenterX = panel.getCenterX();
			int oldCenterY = panel.getCenterY();

			int newWidth = totalWidth + panel.getMargin() * 2;
			int newHeight = totalHeight + panel.getMargin() * 2;

			panel.setWidth(totalWidth + panel.getMargin() * 2);
			panel.setHeight(totalHeight + panel.getMargin() * 2);

			panel.setX(oldCenterX - newWidth / 2);
			panel.setY(oldCenterY - newHeight / 2);
		}
	}
}
