package nl.basvs.lib.gui;

import java.util.HashMap;
import java.util.Stack;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.element.GuiPanel;
import nl.basvs.lib.gui.element.AbstractGuiElement.GuiStatus;

import org.newdawn.slick.SlickException;

/**
 * A helper class for managing graphical user interface panels.
 */
public class GuiManager {

	private InputHelper input;

	private HashMap<String, GuiPanel> panels = new HashMap<String, GuiPanel>();
	private Stack<GuiPanel> activateStack = new Stack<GuiPanel>();
	private Stack<GuiPanel> deactivateStack = new Stack<GuiPanel>();

	private GuiCursor cursor = null;
	private boolean cursorVisible;

	/**
	 * Add a panel to the GuiManager collection.
	 * 
	 * @param id
	 * @param panel
	 */
	public void addPanel(String id, GuiPanel panel) {
		if (panel == null) {
			throw new RuntimeException("Panel may not be null");
		}
		panels.put(id, panel);
	}

	/**
	 * Remove a panel from the GuiManager collection.
	 * 
	 * @param id
	 */
	public void removePanel(String id) {
		if (!panels.containsKey(id)) {
			throw new RuntimeException("Panel does not exist");
		}
		panels.remove(id);
	}

	/**
	 * Get a panel from the GuiManager collection.
	 * 
	 * @param id
	 * @return
	 */
	public GuiPanel getPanel(String id) {
		if (!panels.containsKey(id)) {
			throw new RuntimeException("Panel does not exist");
		}
		return panels.get(id);
	}

	/**
	 * Check if the cursor is visible.
	 * 
	 * @return
	 */
	public boolean isCursorVisible() {
		return cursorVisible && (cursor != null);
	}

	/**
	 * Set the cursor visibility.
	 * 
	 * @param cursorVisible
	 */
	public void setCursorVisible(boolean cursorVisible) {
		this.cursorVisible = cursorVisible;
	}

	/**
	 * Set the cursor image to use.
	 * 
	 * @param cursor
	 */
	public void setCursor(GuiCursor cursor) {
		this.cursor = cursor;
	}

	public GuiManager(InputHelper input) throws SlickException {
		this.input = input;
		cursorVisible = true;
	}

	/**
	 * Activate a panel. This will not come into effect until update() is executed.
	 * 
	 * @param id
	 *            id of the panel to activate
	 * @param deactivateRest
	 *            deactivates all other panels when true
	 */
	public void activate(String id, boolean deactivateRest) {
		if (!panels.containsKey(id)) {
			throw new RuntimeException("Panel does not exist");
		}
		if (deactivateRest) {
			for (String panelId : panels.keySet()) {
				if (!panelId.equals(id)) {
					deactivate(panelId);
				}
			}
		}
		activateStack.push(panels.get(id));
	}

	/**
	 * Deactivate a panel. This will not come into effect until update() is executed.
	 * 
	 * @param id
	 *            id of the panel to deactivate.
	 */
	public void deactivate(String id) {
		if (!panels.containsKey(id)) {
			throw new RuntimeException("Panel does not exist");
		}
		deactivateStack.push(panels.get(id));
	}

	/**
	 * Render all panels on the screen.
	 * 
	 * @throws Exception
	 */
	public void render() throws Exception {
		for (GuiPanel panel : panels.values()) {
			panel.render();
		}
		// Only render the cursor if its set to visible and a cursor image was set
		if (cursorVisible && cursor != null) {
			cursor.render(input.getMouseX(), input.getMouseY());
		}
	}

	/**
	 * Update all panels (i.e. respond to user input and activate / deactivate panels).
	 * 
	 * @param delta
	 * @throws Exception
	 */
	public void update(int delta) throws Exception {
		for (GuiPanel panel : panels.values()) {
			panel.update(delta);
		}

		// (de)activate panels as requested
		while (!deactivateStack.isEmpty()) {
			deactivateStack.pop().setStatus(GuiStatus.TransitionOff);
		}
		while (!activateStack.isEmpty()) {
			GuiPanel activatePanel = activateStack.pop();
			activatePanel.setStatus(GuiStatus.TransitionOn);
			activatePanel.preActivation();
		}
	}
}
