package nl.basvs.lib.gui;

import nl.basvs.lib.gui.element.AbstractGuiElement;

/**
 * Interface for listeners that receive Gui element events, such as button clicks.
 */
public interface IGuiListener {
	public void action(AbstractGuiElement origin) throws Exception;
}
