package nl.basvs.lib.gui.transition;

import org.newdawn.slick.GameContainer;

/**
 * A transition that "folds" out the panel horizontally.
 */
public class ScaleToCenterTransition extends AbstractGuiTransitionEffect {

	public ScaleToCenterTransition(GameContainer container) {
		super(container);
	}

	@Override
	public void setupEffect(float transitionFactor) {
		getContainer().getGraphics().scale(transitionFactor * transitionFactor, transitionFactor * transitionFactor);
	}
}
