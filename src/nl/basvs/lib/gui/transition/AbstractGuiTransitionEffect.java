package nl.basvs.lib.gui.transition;

import org.newdawn.slick.GameContainer;

/**
 * A transition helper class that sets up graphics transformation based on a transition factor..
 */
public abstract class AbstractGuiTransitionEffect {
	private GameContainer container;

	protected GameContainer getContainer() {
		return container;
	}

	public AbstractGuiTransitionEffect(GameContainer container) {
		this.container = container;
	}

	public abstract void setupEffect(float transitionFactor);

	public void clearEffect() {
		container.getGraphics().resetTransform();
	}
}
