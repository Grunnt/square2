package nl.basvs.lib.gui.transition;

import org.newdawn.slick.GameContainer;

/**
 * A transition that "folds" out the panel horizontally.
 */
public class GuiHorizontalFoldTransition extends AbstractGuiTransitionEffect {

	public GuiHorizontalFoldTransition(GameContainer container) {
		super(container);
	}

	@Override
	public void setupEffect(float transitionFactor) {
		getContainer().getGraphics().scale(transitionFactor * transitionFactor, 1f);
	}
}
