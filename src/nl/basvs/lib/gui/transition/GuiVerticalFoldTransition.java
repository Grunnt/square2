package nl.basvs.lib.gui.transition;

import org.newdawn.slick.GameContainer;

/**
 * A transition that "folds" out the panel vertically.
 */
public class GuiVerticalFoldTransition extends AbstractGuiTransitionEffect {

	public GuiVerticalFoldTransition(GameContainer container) {
		super(container);
	}

	@Override
	public void setupEffect(float transitionFactor) {
		getContainer().getGraphics().scale(1f, transitionFactor * transitionFactor);
	}
}
