package nl.basvs.lib.gui.transition;

import org.newdawn.slick.GameContainer;

/**
 * A transition that rotates the panel into the screen from a specific corner.
 */
public class GuiRotateTransition extends AbstractGuiTransitionEffect {

	private int corner;

	public GuiRotateTransition(GameContainer container, int corner) {
		super(container);
		this.corner = corner;
	}

	@Override
	public void setupEffect(float transitionFactor) {
		switch (corner) {
		case 0:
			getContainer().getGraphics().rotate(getContainer().getScreenWidth(), 0, (1f - transitionFactor) * 90f);
			break;
		case 1:
			getContainer().getGraphics().rotate(getContainer().getScreenWidth(), getContainer().getScreenHeight(),
					(1f - transitionFactor) * 90f);
			break;
		case 2:
			getContainer().getGraphics().rotate(0, getContainer().getScreenHeight(), (1f - transitionFactor) * 90f);
			break;
		default:
			getContainer().getGraphics().rotate(0, 0, (1f - transitionFactor) * 90f);
			break;
		}
	}
}
