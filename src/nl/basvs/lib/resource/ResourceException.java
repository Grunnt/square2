package nl.basvs.lib.resource;

@SuppressWarnings("serial")
public class ResourceException extends Exception {
	public ResourceException(String message) {
		super(message);
	}
}