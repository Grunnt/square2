package nl.basvs.lib.resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;

/**
 * Resource manager that will preload all resources listed in a resource list file. This class can function as a single
 * place to manage all game resources.
 */
public class ResourceManager {

	// The single instance of the resource manager (null until initialized).
	private static ResourceManager singleton = null;

	// Resource maps.
	private HashMap<String, Image> images = new HashMap<String, Image>();
	private HashMap<String, Sound> sounds = new HashMap<String, Sound>();
	private HashMap<String, UnicodeFont> fonts = new HashMap<String, UnicodeFont>();

	// Resource file name extensions that we will load.
	private static List<String> IMAGE_TYPES = new ArrayList<String>();
	static {
		IMAGE_TYPES.add("png");
		IMAGE_TYPES.add("jpg");
	}

	private static List<String> SOUND_TYPES = new ArrayList<String>();
	static {
		SOUND_TYPES.add("wav");
	}

	private static List<String> FONT_TYPES = new ArrayList<String>();
	static {
		FONT_TYPES.add("hiero");
	}

	/**
	 * Get an image resource.
	 * 
	 * @param ref
	 * @return
	 * @throws SlickException
	 */
	public Image getImage(String ref) throws SlickException {
		if (!images.containsKey(ref))
			throw new SlickException("Image with reference '" + ref + "' not loaded.");
		return images.get(ref);
	}

	/**
	 * Get a sound resource.
	 * 
	 * @param ref
	 * @return
	 * @throws SlickException
	 */
	public Sound getSound(String ref) throws SlickException {
		if (!sounds.containsKey(ref))
			throw new SlickException("Sound with reference '" + ref + "' not loaded.");
		return sounds.get(ref);
	}

	/**
	 * Get a font resource.
	 * 
	 * @param ref
	 * @return
	 * @throws SlickException
	 */
	public UnicodeFont getFont(String ref) throws SlickException {
		if (!fonts.containsKey(ref))
			throw new SlickException("Font with reference '" + ref + "' not loaded.");
		return fonts.get(ref);
	}

	/**
	 * Initialize the resource manager. Must be called once at game initialization (e.g. in StateBasedGame.init()).
	 * 
	 * @throws ResourceException
	 * @throws IOException
	 */
	public static void initialize(String resourceFile) throws SlickException, IOException, ResourceException {
		// Initialize the instance
		singleton = new ResourceManager(resourceFile);
	}

	/**
	 * Return the resource manager object. The resource manager must have been initialized before calling this.
	 * 
	 * @return
	 * @throws SlickException
	 */
	public static ResourceManager getInstance() throws SlickException {
		if (singleton == null) {
			throw new SlickException("ResourceManager has not yet been initialized");
		}
		return singleton;
	}

	/**
	 * Load all resources from the specified directories.
	 * 
	 * @param resourceFile
	 * @throws SlickException
	 * @throws IOException
	 * @throws ResourceException
	 */
	public ResourceManager(String resourceListFileName) throws SlickException, IOException, ResourceException {

		File resourceListFile = new File(resourceListFileName);

		// Parse the resource file
		List<File> resources = parseResourceFile(resourceListFile);

		// Load each resource
		for (File resource : resources) {
			loadResource(resourceListFile.getParentFile(), resource);
		}
	}

	/**
	 * Parse the resource list file, and check if all files are present.
	 * 
	 * @param resourceListFile
	 * @return A list of resource files.
	 * @throws IOException
	 * @throws ResourceException
	 */
	private List<File> parseResourceFile(File resourceListFile) throws IOException, ResourceException {

		List<File> result = new ArrayList<File>();

		// Open the resource list file
		BufferedReader reader = new BufferedReader(new FileReader(resourceListFile));
		String line = null;

		// Parse the file line by line
		while ((line = reader.readLine()) != null) {
			line = line.trim();

			// Skip empty lines and comments
			if (!(line.equals("") || line.startsWith("#"))) {

				// Get the absolute file path
				File absoluteFile = new File(resourceListFile.getParentFile(), line);

				// Check if the line refers to an existing file
				if (!absoluteFile.exists() || !absoluteFile.isFile()) {
					throw new ResourceException("Resource not found or not a file: " + absoluteFile.getAbsolutePath());
				}

				// Remember the file path relative to the resource list file's location
				result.add(new File(line));
			}
		}

		// Return the list of resources we found
		return result;
	}

	/**
	 * Determine the type of the resource file, and load it (if possible).
	 * 
	 * @param dataPath
	 * @param rFile
	 * @throws SlickException
	 * @throws ResourceException
	 */
	private void loadResource(File basePath, File relativeFile) throws SlickException, ResourceException {

		// Get the full path to the resource file itself
		File actualFile = new File(basePath, relativeFile.getPath());

		// Get the file type extension (ie. the part behind the last dot)
		String[] nameParts = relativeFile.getName().split("\\.");
		if (nameParts.length < 2) {
			throw new ResourceException("Resource file has no type extension");
		}
		String typeExtension = nameParts[nameParts.length - 1];

		// Get the file path without extension (relative to dataPath)
		String filePathWithoutExtension = relativeFile.getPath().substring(0,
				relativeFile.getPath().length() - (typeExtension.length() + 1));

		// For a reference we use the relative path of this resource minus its type extension
		String reference = filePathWithoutExtension.replaceAll("\\\\", "/");

		// Now check the file type and try to load it
		if (IMAGE_TYPES.contains(typeExtension)) {

			// This is an image
			Image newImage = new Image(actualFile.getAbsolutePath());
			images.put(reference, newImage);

		} else if (SOUND_TYPES.contains(typeExtension)) {

			// This is a sound
			Sound newSound = new Sound(actualFile.getAbsolutePath());
			sounds.put(reference, newSound);

		} else if (FONT_TYPES.contains(typeExtension)) {

			// This is a font
			File ttfFile = new File(basePath, filePathWithoutExtension + ".ttf");
			// Load the font using 2 files, one .ttf and one .hiero, with identical names
			UnicodeFont newFont = new UnicodeFont(ttfFile.getAbsolutePath(), actualFile.getAbsolutePath());

			// Only load nehe glyphs
			newFont.addNeheGlyphs();
			newFont.loadGlyphs();

			// Add the font to our resources
			fonts.put(reference, newFont);

		}
	}
}
