package nl.basvs.square.menustate;

import java.util.List;

import nl.basvs.lib.resource.ResourceManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

public class HighScoreRenderer {

	private static final float SCORE_CHANGE_INTERVAL = 350;
	private static final float SCORE_DISPLAY_INTERVAL = 3500;

	// The font and color used to draw the score
	private UnicodeFont font;
	private Color color;
	private Color titleColor;

	// Locations of the two highscore lists
	private int leftX, rightX, Y;

	// Pre-created score texts, to avoid garbage collection.
	private String[] texts = new String[HighScores.NUMBER_OF_HIGH_SCORES];

	// Score change parameters
	private boolean changingOff;
	private boolean changingOn;
	private float progress;
	private int currentPosition;

	public HighScoreRenderer(int leftX, int rightX, int Y) throws SlickException {
		this.leftX = leftX;
		this.rightX = rightX;
		this.Y = Y;
		color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
		titleColor = new Color(0.5f, 0.5f, 0.5f, 0.7f);
		font = ResourceManager.getInstance().getFont("gui/textfont");
		changingOn = true;
		changingOff = false;
		progress = 0f;
		currentPosition = 0;
	}

	public void setTexts(HighScores scores) {
		List<HighScoreEntry> scs = scores.getScores();
		for (int s = 0; s < scs.size(); s++) {
			texts[s] = scs.get(s).getName() + " " + scs.get(s).getScore();
		}
	}

	public void update(int delta) {
		if (changingOn) {
			progress += (float) delta / SCORE_CHANGE_INTERVAL;
			if (progress >= 1f) {
				changingOn = false;
				progress = 0f;
			}
		} else if (changingOff) {
			progress += (float) delta / SCORE_CHANGE_INTERVAL;
			if (progress >= 1f) {
				changingOff = false;
				changingOn = true;
				progress = 0f;

				currentPosition = (currentPosition + 5) % HighScores.NUMBER_OF_HIGH_SCORES;
			}
		} else {
			progress += (float) delta / SCORE_DISPLAY_INTERVAL;
			if (progress >= 1f) {
				changingOff = true;
				progress = 0f;
			}
		}
	}

	public void render() {
		int changeMoveDelta = 0;
		if (changingOn) {
			changeMoveDelta = (int) ((1f - progress * progress) * leftX * 1.5f);
		} else if (changingOff) {
			changeMoveDelta = (int) (progress * progress * leftX * 2f);
		}

		renderList(currentPosition, leftX - changeMoveDelta, Y);
		renderList(currentPosition, rightX + changeMoveDelta, Y);
	}

	private void renderList(int size, int x, int y) {
		float lineHeight = font.getLineHeight();
		int tsw = font.getWidth(texts[currentPosition]);
		font.drawString(x - tsw / 2, y, texts[currentPosition], titleColor);
		for (int s = 1; s < 5; s++) {
			int sw = font.getWidth(texts[(currentPosition + s) % HighScores.NUMBER_OF_HIGH_SCORES]);
			font.drawString(x - sw / 2, y + s * lineHeight, texts[(currentPosition + s)
					% HighScores.NUMBER_OF_HIGH_SCORES], color);
		}
	}
}
