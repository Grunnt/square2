package nl.basvs.square.menustate;

public class HighScoreEntry implements Comparable<HighScoreEntry> {
	private String name;
	private int score;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public HighScoreEntry(String name, int score) {
		super();
		this.name = name;
		this.score = score;
	}

	@Override
	public int compareTo(HighScoreEntry o) throws ClassCastException {
		if (!(o instanceof HighScoreEntry)) {
			throw new ClassCastException("A HighScoreEntry object was expected");
		}
		return this.score - o.getScore();
	}
}
