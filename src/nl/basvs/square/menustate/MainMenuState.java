package nl.basvs.square.menustate;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gamestate.AbstractGame;
import nl.basvs.lib.gamestate.AbstractGameState;
import nl.basvs.lib.graphics.Overlay;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.Square2;
import nl.basvs.square.SquareBackground;
import nl.basvs.square.gamestate.SquareGameState;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

/**
 * Square main menu state, which includes options and help menus.
 */
public class MainMenuState extends AbstractGameState implements IGuiMainMenuListener {

	public static final String BUILD_TEXT = "build " + Square2.BUILD_NUMBER;

	// Some helper classes
	private GuiMainMenu guiMainMenu;

	private UnicodeFont font;

	// The Square title image
	private Image bigLogoImage;
	private Image grunntImage;

	// Fancy moving background
	private SquareBackground background;

	// And the score displays
	private HighScoreRenderer highScoreRenderer;

	// A black overlay for transitions
	private Overlay blackOverlay;

	/**
	 * Create a new main menu state.
	 * 
	 * @param container
	 * @param game
	 * @param input
	 * @throws Exception
	 */
	public MainMenuState(GameContainer container, AbstractGame game, InputHelper input) throws Exception {
		super(container, game, input);

		guiMainMenu = new GuiMainMenu(container, input);
		guiMainMenu.addListener(this);

		// Load resources
		ResourceManager rm = ResourceManager.getInstance();
		bigLogoImage = rm.getImage("logobig");
		grunntImage = rm.getImage("basvs");
		font = rm.getFont("gui/textfont");

		// Setup a fancy moving background
		background = new SquareBackground(container.getWidth(), container.getHeight());

		blackOverlay = new Overlay(Color.black, container);

		// And the high score displays
		highScoreRenderer = new HighScoreRenderer(200, container.getWidth() - 200, 400);
	}

	@Override
	public void play() throws SlickException {
		// The play option was selected in the main menu, so change to game state
		SquareGameState gameState = (SquareGameState) game.getState(Square2.STATE_GAME);
		gameState.setup();
		game.activate(Square2.STATE_GAME, true);
	}

	@Override
	public void exitSelected() throws Exception {
		// Transition this state off and exit
		setExitWhenInactive(true);
		setState(State.TransitionOff);
	};

	@Override
	protected void doRender() throws Exception {

		// Render the moving background
		background.render();

		// Show the title image and logo image
		bigLogoImage.draw(container.getWidth() / 2 - bigLogoImage.getWidth() / 2, 30);
		grunntImage
				.draw(container.getWidth() - grunntImage.getWidth(), container.getHeight() - grunntImage.getHeight());

		// Render score displays
		highScoreRenderer.render();

		// Draw the menu
		guiMainMenu.render();

		// Draw build number
		float lineHeight = font.getLineHeight();
		font.drawString(10, container.getHeight() - lineHeight - 5, BUILD_TEXT, Color.cyan);

		if (isTransitioning()) {
			blackOverlay.draw(1f - getTransitionFactor());
		}
	}

	@Override
	protected void doUpdate(int delta) throws Exception {
		// Update the moving background
		background.update(delta);

		// Update the highscore displays
		highScoreRenderer.update(delta);

		// Update the gui (also handle input)
		guiMainMenu.update(delta);
	}

	@Override
	protected void activating() {
		guiMainMenu.reset();
		highScoreRenderer.setTexts(Square2.getScores());
	}

	@Override
	protected void deactivating() {
		// Do nothing
	}

	@Override
	protected void activated() throws Exception {
		// Do nothing
	}

	@Override
	protected void deactivated() throws Exception {
		// Do nothing
	}
}
