package nl.basvs.square.menustate;

import org.newdawn.slick.SlickException;

public interface IGuiMainMenuListener {
	public void play() throws SlickException;

	public void exitSelected() throws Exception;
}
