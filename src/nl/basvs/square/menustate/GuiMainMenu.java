package nl.basvs.square.menustate;

import java.util.ArrayList;
import java.util.List;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gui.GuiCursor;
import nl.basvs.lib.gui.GuiManager;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;
import nl.basvs.lib.gui.LayoutHelper;
import nl.basvs.lib.gui.LayoutHelper.HorizontalAlignment;
import nl.basvs.lib.gui.LayoutHelper.VerticalAlignment;
import nl.basvs.lib.gui.element.AbstractGuiElement;
import nl.basvs.lib.gui.element.AbstractGuiElement.GuiStatus;
import nl.basvs.lib.gui.element.GuiButton;
import nl.basvs.lib.gui.element.GuiPanel;
import nl.basvs.lib.gui.transition.GuiHorizontalFoldTransition;
import nl.basvs.lib.gui.transition.GuiRotateTransition;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.Square2;
import nl.basvs.square.SquareSettings;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * Gui definition for the Square main menu.
 */
public class GuiMainMenu implements IGuiListener {

	// Listeners that we will notify of main menu events, such as when the "play" button is clicked
	private List<IGuiMainMenuListener> listeners = new ArrayList<IGuiMainMenuListener>();

	// Identifiers for the buttons, so we can find out which button was clicked.
	private static final String ID_MAIN_MENU_PLAY_BUTTON = "mainPlay";
	private static final String ID_MAIN_MENU_OPTIONS_BUTTON = "mainOptions";
	private static final String ID_MAIN_MENU_EXIT_BUTTON = "mainExit";
	private static final String ID_OPTIONS_MENU_CLOSED = "optionsClosed";
	private static final String ID_GAME_MENU_BUTTON = "gameMenu";

	// Some helper classes
	private GuiManager guiManager;
	private InputHelper input;
	private GameContainer container;
	private GuiOptionsMenu optionsMenu;

	private Sound clickSound;

	/**
	 * Add a listener that will handle main menu events (such as play button pressed).
	 * 
	 * @param listener
	 */
	public void addListener(IGuiMainMenuListener listener) {
		listeners.add(listener);
	}

	/**
	 * Create the main menu Gui.
	 * 
	 * @param container
	 * @param input
	 * @throws SlickException
	 */
	public GuiMainMenu(GameContainer container, InputHelper input) throws SlickException {
		this.container = container;
		this.input = input;

		// We use a gui manager to handle the main gui panels.
		guiManager = new GuiManager(input);

		ResourceManager rm = ResourceManager.getInstance();
		clickSound = rm.getSound("audio/action");

		guiManager.setCursor(new GuiCursor(rm.getImage("gui/cursor"), 5, 1));

		// Setup the gui elements
		buildGui(container);

	}

	/**
	 * Render the gui.
	 * 
	 * @throws Exception
	 */
	public void render() throws Exception {
		guiManager.render();
	}

	/**
	 * Update the gui, handle input.
	 * 
	 * @param delta
	 * @throws Exception
	 */
	public void update(int delta) throws Exception {
		guiManager.update(delta);
	}

	@Override
	public void action(AbstractGuiElement origin) throws Exception {
		if (origin instanceof GuiOptionsMenu) {
			if (origin.getId().equals(ID_OPTIONS_MENU_CLOSED)) {
				// The options menu is being exited, so we get the new settings
				SquareSettings.soundVolume = optionsMenu.getSoundVolume();
				boolean fullScreenSetting = optionsMenu.isFullScreenSelected();

				// Change fullscreen setting if required
				if (container.isFullscreen() != fullScreenSetting) {
					container.setFullscreen(fullScreenSetting);
				}

				// Go back to the main menu
				guiManager.activate("main", true);
			}
		} else if (origin instanceof GuiButton) {

			// A button was pressed
			GuiButton button = (GuiButton) origin;

			// Make a button click sound
			clickSound.play(1f, SquareSettings.soundVolume);

			if (button.getId().equals(ID_MAIN_MENU_OPTIONS_BUTTON)) {
				// The main menu options button was clicked, so show the options menu
				guiManager.activate("options", true);

			} else if (button.getId().equals(ID_MAIN_MENU_PLAY_BUTTON)) {
				// Play the game, notify the listeners
				for (IGuiMainMenuListener listener : listeners) {
					listener.play();
				}
			} else if (button.getId().equals(ID_GAME_MENU_BUTTON)) {
				// Return to the main menu
				guiManager.activate("main", true);

			} else if (button.getId().equals(ID_MAIN_MENU_EXIT_BUTTON)) {

				// Notify the listeners
				for (IGuiMainMenuListener listener : listeners) {
					listener.exitSelected();
				}
			}
		}
	}

	/**
	 * Setup the gui elements.
	 * 
	 * @param listener
	 * @param container
	 * @throws SlickException
	 */
	private void buildGui(GameContainer container) throws SlickException {
		GuiStyle style = Square2.getStyle();

		// Calculate the location of the menus
		int menuCenterX = container.getWidth() / 2;
		int menuCenterY = container.getHeight() / 2 + 120;

		// Setup a main panel, with some controls on it
		GuiPanel mainMenuPanel = new GuiPanel(null, menuCenterX - 125, menuCenterY - 130, 250, 260, 8, style);
		mainMenuPanel.setTransitionEffect(new GuiHorizontalFoldTransition(container));

		GuiButton mainPlayButton = new GuiButton(input, ID_MAIN_MENU_PLAY_BUTTON, "Play", 1, 1, 200, 50, 0, style);
		mainPlayButton.addListener(this);
		mainMenuPanel.addChild(mainPlayButton);

		GuiButton mainOptionsButton = new GuiButton(input, ID_MAIN_MENU_OPTIONS_BUTTON, "Options", 1, 1, 200, 50, 0,
				style);
		mainOptionsButton.addListener(this);
		mainMenuPanel.addChild(mainOptionsButton);

		GuiButton mainExitButton = new GuiButton(input, ID_MAIN_MENU_EXIT_BUTTON, "Exit", 1, 1, 200, 50, 0, style);
		mainExitButton.addListener(this);
		mainMenuPanel.addChild(mainExitButton);

		LayoutHelper.layoutRows(mainMenuPanel, HorizontalAlignment.Centered, 3, VerticalAlignment.Centered, 3, true);

		// Setup an options panel
		optionsMenu = new GuiOptionsMenu(ID_OPTIONS_MENU_CLOSED, menuCenterX, menuCenterY, style, input);
		optionsMenu.addListener(this);
		optionsMenu.setTransitionEffect(new GuiRotateTransition(container, 2));

		guiManager.addPanel("main", mainMenuPanel);
		guiManager.addPanel("options", optionsMenu);
	}

	/**
	 * Go back to the main menu after completing a game.
	 */
	public void reset() {
		// Switch the main menu to active and the play menu to inactive
		guiManager.getPanel("main").setStatus(GuiStatus.Active);
	}
}
