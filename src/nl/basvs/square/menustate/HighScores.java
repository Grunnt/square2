package nl.basvs.square.menustate;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple game highscore management class.
 */
public class HighScores {
	public static final int NUMBER_OF_HIGH_SCORES = 20;
	// A list of 20 highscores, index 0 is the highest and NUMBER_OF_HIGH_SCORES the lowest
	private List<HighScoreEntry> scores;
	private File scoreFile;

	public List<HighScoreEntry> getScores() {
		return scores;
	}

	public HighScores(File scoreFile) throws IOException {
		scores = new ArrayList<HighScoreEntry>();
		this.scoreFile = scoreFile;

		// Try to open and read the file
		try {
			FileInputStream fstream = new FileInputStream(scoreFile);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String readLine;
			while ((readLine = br.readLine()) != null) {
				String[] parts = readLine.split(",");
				scores.add(new HighScoreEntry(parts[0], Integer.parseInt(parts[1])));
			}
			in.close();

		} catch (Exception e) {
			// Failed to read the highscores, so we create a new one
			for (int i = 0; i < NUMBER_OF_HIGH_SCORES; i++) {
				scores.add(new HighScoreEntry("square2", i + 1));
			}
			Collections.sort(scores, Collections.reverseOrder());
			save();
		}
	}

	/**
	 * Save the scores to the score file.
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		// We could not open the score file, so we generate a new one
		FileWriter outFile;
		outFile = new FileWriter(scoreFile);
		PrintWriter out = new PrintWriter(outFile);

		for (HighScoreEntry entry : scores) {
			out.println(entry.getName() + "," + entry.getScore());
		}

		out.close();
	}

	/**
	 * Check whether or not a score is a highscore.
	 * 
	 * @param score
	 * @return true if the score is higher than the lowest highscore.
	 */
	public boolean isHighScore(int score) {
		return (score > scores.get(4).getScore());
	}

	/**
	 * Get the position in which a new score would be inserted in the high score list.
	 * 
	 * @param score
	 * @return -1 if this is not a highscore, else a position from 0 to 4
	 */
	public int getHighScorePosition(int score) {
		// Find the position in which this score would be inserted (i.e. the
		// first index which contains a lower score)
		for (int s = 0; s < NUMBER_OF_HIGH_SCORES; s++) {
			if (score > scores.get(s).getScore())
				return s;
		}
		// Return -1 if this is not a highscore (i.e. not higher than any score
		// in the list)
		return -1;
	}

	/**
	 * Insert a score in the highscore-list. If the score is not a highscore, it will simply be ignored.
	 * 
	 * @param entry
	 */
	public void insertHighScore(HighScoreEntry entry) {
		// Add the new entry
		scores.add(entry);
		// Put it in the correct place
		Collections.sort(scores, Collections.reverseOrder());
		// Now cut off the lowest score
		scores = scores.subList(0, NUMBER_OF_HIGH_SCORES);
	}
}
