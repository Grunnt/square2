package nl.basvs.square;

import java.io.File;
import java.io.IOException;

import nl.basvs.lib.ErrorDialog;
import nl.basvs.lib.SettingsHelper;
import nl.basvs.lib.gamestate.AbstractGame;
import nl.basvs.lib.graphics.GraphicsHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.PartitionImage;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.gamestate.SquareGameState;
import nl.basvs.square.loadingstate.LoadingState;
import nl.basvs.square.menustate.HighScores;
import nl.basvs.square.menustate.MainMenuState;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 * Main entry point for the application.
 */
public class Square2 extends AbstractGame {

	public static final int BUILD_NUMBER = 48;

	public static final String STATE_LOADING = "loading";
	public static final String STATE_MAIN_MENU = "mainmenu";
	public static final String STATE_GAME = "game";

	public static final String SETTING_SOUND_VOLUME = "soundVolume";
	public static final String SETTING_FULLSCREEN = "fullScreen";

	private static HighScores scores;

	public static GuiStyle style;

	public static HighScores getScores() {
		return scores;
	}

	public static GuiStyle getStyle() {
		return style;
	}

	public Square2() throws SlickException {
		super("Square v2");
	}

	public static void main(String[] args) {
		try {

			// Load the settings
			if (!SettingsHelper.initialize("square.cfg")) {
				// Loading the settings file failed, so we setup some default
				// settings
				SettingsHelper settings = SettingsHelper.getInstance();
				settings.putSetting(SETTING_SOUND_VOLUME, 1f);
				settings.putSetting(SETTING_FULLSCREEN, false);
				try {
					settings.save();
				} catch (IOException e) {
					throw new SlickException(
							"Could not load existing settings file or create new default settings file!", e);
				}
			}

			SquareSettings.soundVolume = (Float) SettingsHelper.getInstance().getSetting(SETTING_SOUND_VOLUME);
			SquareSettings.fullScreen = (Boolean) SettingsHelper.getInstance().getSetting(SETTING_FULLSCREEN);

			// Load the highscores for all level sizes
			scores = new HighScores(new File("scores.score"));

			// Set display mode to the one selected in the settings
			boolean fullScreen = (Boolean) SettingsHelper.getInstance().getSetting(SETTING_FULLSCREEN);
			AppGameContainer app = new AppGameContainer(new Square2(), 1024, 768, fullScreen);
			app.setVerbose(true);
			app.setClearEachFrame(true);
			// Limit maximum framerate
			app.setTargetFrameRate(60);
			app.setShowFPS(false);
			app.start();

		} catch (Throwable t) {
			ErrorDialog.show(null, "Exception during startup", t);
			System.exit(0);
		}
	}

	@Override
	protected void initialize() {
		try {

			// The screen should be cleared in black
			container.getGraphics().setBackground(Color.black);

			// Hide the default mouse cursor
			container.setMouseCursor(GraphicsHelper.generateImage(2, 2, 255, 255, 255, 0), 0, 0);

			container.setIcons(new String[] { "data/16x16.png", "data/24x24.png", "data/32x32.png", "data/64x64.png" });

			// Start with the loading state
			addState(STATE_LOADING, new LoadingState(container, this, input));
			activate(STATE_LOADING, true);

		} catch (Throwable t) {
			ErrorDialog.show(null, "Exception during initialization", t);
			System.exit(0);
		}
	}

	/**
	 * Load all resources and initialize all states. This should be called after the load screen is displayed.
	 * 
	 * @throws Exception
	 */
	public void delayedInit(GameContainer container) throws Exception {

		// Load all resources
		ResourceManager.initialize("data/square.res");

		// Setup a graphical style for the gui
		style = createStyle();

		// Initialize all game states
		addState(STATE_MAIN_MENU, new MainMenuState(container, this, input));
		addState(STATE_GAME, new SquareGameState(container, this, input));
	}

	/**
	 * Setup the user interface graphical style.
	 * 
	 * @return
	 * @throws SlickException
	 */
	private static GuiStyle createStyle() throws SlickException {

		ResourceManager resourceManager = ResourceManager.getInstance();

		GuiStyle style = new GuiStyle();
		style.setPanelColor(new Color(33, 46, 125, 200));
		style.setElementColor(new Color(33, 89, 125, 200));
		style.setHighLightColor(new Color(33, 89, 125, 255));
		style.setSelectorColor(new Color(142, 255, 255, 200));
		style.setTextColor(new Color(142, 255, 255, 200));
		style.setTitleColor(new Color(142, 255, 255, 255));
		style.setPanelImage(new PartitionImage(resourceManager.getImage("gui/panel"), 18));
		style.setButtonImage(new PartitionImage(resourceManager.getImage("gui/button"), 18));
		style.setTextFieldImage(new PartitionImage(resourceManager.getImage("gui/textfield"), 18));
		style.setCheckBoxImage(new PartitionImage(resourceManager.getImage("gui/checkbox"), 18));
		style.setCheckBoxCheckedImage(resourceManager.getImage("gui/check"));
		style.setSliderImage(new PartitionImage(resourceManager.getImage("gui/slider"), 18));
		style.setSliderMoverImage(new PartitionImage(resourceManager.getImage("gui/slidermover"), 18));
		style.setTextFont(resourceManager.getFont("gui/textfont"));
		style.setTitleFont(resourceManager.getFont("gui/textfont"));
		return style;
	}
}