package nl.basvs.square;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.SlickException;

public class SquareBackground {
	private List<SquareBackgroundItem> items = new ArrayList<SquareBackgroundItem>();
	private Random random = new Random();

	public SquareBackground(int screenWidth, int screenHeight) throws SlickException {
		for (int s = 0; s < 75; s++) {
			SquareBackgroundItem item = new SquareBackgroundItem(random, screenWidth, screenHeight);
			items.add(item);
			item.randomize(true);
		}
	}

	public void update(int delta) {
		for (int s = 0; s < items.size(); s++) {
			items.get(s).update(delta);
		}
	}

	public void render() {
		for (int s = 0; s < items.size(); s++) {
			items.get(s).render();
		}
	}
}