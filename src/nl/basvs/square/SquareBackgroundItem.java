package nl.basvs.square;

import java.util.Random;

import nl.basvs.lib.resource.ResourceManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SquareBackgroundItem {

	private int x, y;
	private float progress;
	private Image image;
	private Color color;
	private float duration;
	private Random random;
	private int screenWidth, screenHeight;

	public SquareBackgroundItem(Random random, int screenWidth, int screenHeight) throws SlickException {
		this.random = random;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		image = ResourceManager.getInstance().getImage("square");
		color = new Color(1f, 1f, 1f, 1f);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}

	public Color getColor() {
		return color;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public void randomize(boolean inProgress) {
		setDuration(2500 + random.nextInt(1500));
		setX(random.nextInt(screenWidth));
		setY(random.nextInt(screenHeight));
		if (inProgress) {
			setProgress(random.nextFloat());
		} else {
			setProgress(0f);
		}
		color.a = 0.25f - (progress / 4f);
	}

	public void update(int delta) {
		progress += (float) delta / duration;
		if (progress >= 1f) {
			randomize(false);
		} else {
			color.a = 0.25f - (progress / 4f);
		}
	}

	public void render() {
		int width = (int) (((float) image.getWidth() * progress) / 2f);
		image.draw(x - width, y - width, progress, color);
	}
}
