package nl.basvs.square.loadingstate;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gamestate.AbstractGame;
import nl.basvs.lib.gamestate.AbstractGameState;
import nl.basvs.lib.graphics.Overlay;
import nl.basvs.square.Square2;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;

/**
 * A simple state that fades in a "loading" message, initiates resource loading, transitions to a new state.
 */
public class LoadingState extends AbstractGameState {

	// Visual data
	private Image loadingImage;

	// Properties for handling fade in and fade out
	private boolean loadComplete;
	private boolean skipNextUpdate;

	// A black overlay for transitions
	private Overlay blackOverlay;

	public LoadingState(GameContainer container, AbstractGame game, InputHelper input) throws Exception {
		super(container, game, input);

		loadingImage = new Image("data/loading.png");

		blackOverlay = new Overlay(Color.black, container);
	}

	@Override
	protected void doRender() throws Exception {
		// Draw the loading message
		loadingImage.draw(container.getWidth() / 2 - loadingImage.getWidth() / 2, container.getHeight() / 2
				- loadingImage.getHeight() / 2);

		if (isTransitioning()) {
			blackOverlay.draw(1f - getTransitionFactor());
		}
	}

	@Override
	protected void doUpdate(int delta) throws Exception {

		// We skip one update after the resource loading took place, since this loop will have a very high delta.
		if (skipNextUpdate) {

			// We skipped one update loop, no need to skip more
			skipNextUpdate = false;

		} else {

			if (loadComplete) {
				// Now go to the main menu
				game.activate(Square2.STATE_MAIN_MENU, true);
			}
		}
	}

	@Override
	protected void activating() {
		loadComplete = false;
		skipNextUpdate = false;
	}

	@Override
	protected void deactivating() {
		// Do nothing
	}

	@Override
	protected void activated() throws Exception {
		// The load state has transitioned in, so we can start loading resources and init the other states
		((Square2) game).delayedInit(container);

		// Now fade out again, after skipping an update loop
		loadComplete = true;
		skipNextUpdate = true;
	}

	@Override
	protected void deactivated() {
		// Do nothing
	}
}