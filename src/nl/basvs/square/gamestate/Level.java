package nl.basvs.square.gamestate;

import java.util.Random;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.SquareSettings;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * A Square game level, which is basically the game itself including rendering, handling input, etcetera.
 */
public class Level {

	public static final float GAMEOVER_ANIMATION_INTERVAL = 400f;
	public static final float FALL_MAX_INTERVAL = 400f;
	public static final float FALL_INTERVAL_DECREASE = 0.0007f;
	public static final float FALL_MIN_INTERVAL = 50f;

	public static final int LEVEL_WIDTH = 640;
	public static final int LEVEL_HEIGHT = 640;

	public static final int LEVEL_WIDTH_BLOCKS = 16;
	public static final int LEVEL_HEIGHT_BLOCKS = 16;

	// The color palette for blocks.
	public static final Color[] BLOCK_COLORS = { new Color(25, 255, 25), new Color(255, 150, 0) };
	public Color[] BLOCK_COLORS_TRANSPARENT;

	// Block images, stored in a (fast) enum map for convenience
	private Image blockImage;

	// A "pool" of unused block objects, to avoid garbage collection
	private BlockPool blockPool;

	// The level itself, consisting of a matrix of blocks of the maximum size of
	// the level
	private Block[][] levelBlocks;

	// Input helper for controls specific to level
	private LevelInputHelper input;

	// If the game is over (i.e. no new blocks could be placed)
	private boolean gameOver;
	private boolean paused;

	// Sound effects
	private Sound rotateSound;
	private Sound dropSound;
	private Sound completeSound;
	private Sound bonusSound;

	// For generating random tetrominos
	private Random random = new Random();

	// For adding points to the score
	private ScoreCounter score;

	// Current and next tetrominos
	private Tetromino currentTetromino;
	private TetrominoPreview tetrominoPreview;

	// Fall speed of blocks: time in ms it takes to fall one block
	private float currentFallInterval;

	// Top-left coordinate in pixels of the level within the screen
	private int levelX, levelY;

	/**
	 * Get a block at a specific position.
	 * 
	 * @return
	 */
	public Block getBlock(int x, int y) {
		return levelBlocks[x][y];
	}

	/**
	 * Set a block at a specific position.
	 * 
	 * @return
	 */
	public void setBlock(int x, int y, Block block) {
		levelBlocks[x][y] = block;
	}

	/**
	 * Check if its game over.
	 * 
	 * @return
	 */
	public boolean isGameOver() {
		return gameOver;
	}

	/**
	 * Pause the level.
	 * 
	 * @return
	 */
	public boolean isPaused() {
		return paused;
	}

	/**
	 * Pause the level.
	 * 
	 * @param paused
	 */
	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	/**
	 * Get the tetromino rotation sound.
	 * 
	 * @return
	 */
	public Sound getRotateSound() {
		return rotateSound;
	}

	public Sound getDropSound() {
		return dropSound;
	}

	public Sound getCompleteSound() {
		return completeSound;
	}

	public Sound getBonusSound() {
		return bonusSound;
	}

	/**
	 * The time it takes to fall one block down.
	 * 
	 * @return
	 */
	public float getCurrentFallInterval() {
		return currentFallInterval;
	}

	/**
	 * Get the input handler.
	 * 
	 * @return
	 */
	public LevelInputHelper getInput() {
		return input;
	}

	/**
	 * Get the pool of unused blocks.
	 * 
	 * @return
	 */
	public BlockPool getBlockPool() {
		return blockPool;
	}

	/**
	 * Get the current block image.
	 * 
	 * @return
	 */
	public Image getBlockImage() {
		return blockImage;
	}

	/**
	 * Get the score counter.
	 * 
	 * @return
	 */
	public ScoreCounter getScoreCounter() {
		return score;
	}

	/**
	 * Set the score counter.
	 * 
	 * @param score
	 */
	public void setScoreCounter(ScoreCounter score) {
		this.score = score;
	}

	/**
	 * Get the top-left position of the level within the screen.
	 * 
	 * @return
	 */
	public int getLevelX() {
		return levelX;
	}

	/**
	 * Set the top-left position of the level within the screen.
	 * 
	 * @param levelX
	 */
	public void setLevelX(int levelX) {
		this.levelX = levelX;
	}

	/**
	 * Get the top-left position of the level within the screen.
	 * 
	 * @return
	 */
	public int getLevelY() {
		return levelY;
	}

	/**
	 * Set the top-left position of the level within the screen.
	 * 
	 * @param levelY
	 */
	public void setLevelY(int levelY) {
		this.levelY = levelY;
	}

	public TetrominoPreview getTetrominoPreview() {
		return tetrominoPreview;
	}

	public void setTetrominoPreview(TetrominoPreview tetrominoPreview) {
		this.tetrominoPreview = tetrominoPreview;
	}

	/**
	 * Create a level, which may be reconfigured in different sizes.
	 * 
	 * @param centerX
	 * @param centerY
	 * @param input
	 * @throws SlickException
	 */
	public Level(InputHelper input, int levelX, int levelY) throws SlickException {
		this.input = new LevelInputHelper(input);
		this.levelX = levelX;
		this.levelY = levelY;

		// Load the block images in different sizes
		ResourceManager rm = ResourceManager.getInstance();
		blockImage = rm.getImage("game/blocklarge");

		// Load sound effects
		rotateSound = rm.getSound("audio/dot");
		dropSound = rm.getSound("audio/drop");
		completeSound = rm.getSound("audio/complete");
		bonusSound = rm.getSound("audio/bonus");

		// Setup the block pool
		blockPool = new BlockPool();

		// Create an array of block colors that are transparent
		BLOCK_COLORS_TRANSPARENT = new Color[BLOCK_COLORS.length];
		for (int c = 0; c < BLOCK_COLORS.length; c++) {
			BLOCK_COLORS_TRANSPARENT[c] = BLOCK_COLORS[c].scaleCopy(1f);
			BLOCK_COLORS_TRANSPARENT[c].a = 0.5f;
		}

		// Setup level matrix
		levelBlocks = new Block[LEVEL_WIDTH_BLOCKS][LEVEL_HEIGHT_BLOCKS];

		// Setup the tetromino holder
		currentTetromino = new Tetromino(this);
	}

	/**
	 * Clear the level of all blocks.
	 */
	public void clear() {
		// Return all blocks to the pool and clear the level
		for (int x = 0; x < levelBlocks.length; x++) {
			for (int y = 0; y < levelBlocks[0].length; y++) {
				if (levelBlocks[x][y] != null) {
					blockPool.put(levelBlocks[x][y]);
					levelBlocks[x][y] = null;
				}
			}
		}
	}

	/**
	 * Setup a new empty level.
	 * 
	 * @throws SlickException
	 */
	public void setup() throws SlickException {
		clear();
		gameOver = false;
		int colorIndex = random.nextInt(BLOCK_COLORS.length);
		tetrominoPreview.set(random.nextInt(Tetromino.SHAPE_COORDINATES.length), colorIndex);
		nextTetromino();
		currentFallInterval = FALL_MAX_INTERVAL;
		score.reset();
	}

	/**
	 * Render the level on screen.
	 * 
	 * @throws SlickException
	 */
	public void render() throws SlickException {
		// Render all blocks
		for (int x = 0; x < levelBlocks.length; x++) {
			for (int y = 0; y < levelBlocks[0].length; y++) {
				if (levelBlocks[x][y] != null) {
					if (levelBlocks[x][y].getFlashTimeRemaining() > 0) {
						getBlockImage().drawFlash(levelX + x * getBlockImage().getWidth(),
								levelY + y * getBlockImage().getHeight());

					} else {
						getBlockImage().draw(levelX + x * getBlockImage().getWidth(),
								levelY + y * getBlockImage().getHeight(),
								BLOCK_COLORS[levelBlocks[x][y].getColorIndex()]);
					}
				}
			}
		}

		//
		score.render();

		// Draw the tetromino's
		currentTetromino.render();
		tetrominoPreview.render();
	}

	/**
	 * Update the level status.
	 * 
	 * @param delta
	 * @throws SlickException
	 */
	public void update(int delta) throws SlickException {

		if (!gameOver && !paused) {

			// Update the tetromino
			currentTetromino.update(delta);
			if (currentTetromino.isFallBlocked()) {

				for (int b = 0; b < 4; b++) {
					if (currentTetromino.getBlockY(b) < 0) {
						gameOver = true;
					}
				}

				if (!gameOver) {
					// We hit the bottom, so we need a new tetromino.
					nextTetromino();

					dropSound.play(1f, SquareSettings.soundVolume);

					// Also check for complete lines
					checkForCompleteLines();
				}
			}

			// Speed up the tetromino fall
			currentFallInterval -= FALL_INTERVAL_DECREASE * (float) delta;
			if (currentFallInterval < FALL_MIN_INTERVAL)
				currentFallInterval = FALL_MIN_INTERVAL;
		}

		// Update block animations
		for (int x = 0; x < levelBlocks.length; x++) {
			for (int y = 0; y < levelBlocks[0].length; y++) {
				if (levelBlocks[x][y] != null) {
					levelBlocks[x][y].setFlashTimeRemaining(levelBlocks[x][y].getFlashTimeRemaining() - delta);
					if (levelBlocks[x][y].getFlashTimeRemaining() < 0)
						levelBlocks[x][y].setFlashTimeRemaining(0);
				}
			}
		}

		score.update(delta);
	}

	/**
	 * Check for completed lines, remove them and generate points. Lines of a single color get 3 x points.
	 * 
	 * @throws SlickException
	 */
	private void checkForCompleteLines() throws SlickException {

		boolean playCompleteSound = false;
		boolean playBonusSound = false;

		// First do a check to add the necessary points
		for (int y = 0; y < LEVEL_HEIGHT_BLOCKS; y++) {

			int color = -1;
			if (getBlock(0, y) != null) {
				color = getBlock(0, y).getColorIndex();
			}
			// Check if the line is complete
			boolean lineComplete = true;
			boolean singleColor = true;
			for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
				if (getBlock(x, y) == null) {
					lineComplete = false;
					singleColor = false;
					break;
				} else {
					if (getBlock(x, y).getColorIndex() != color) {
						singleColor = false;
					}
				}
			}

			// If the line is complete, we need to add some points
			if (lineComplete) {
				playCompleteSound = true;
				int scoreValue = 1;
				if (singleColor) {
					scoreValue = 5;
					playBonusSound = true;
				}
				for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
					score.addPoint(x == 0 ? scoreValue : 0, getBlock(x, y).getColorIndex(), levelX + x
							* getBlockImage().getWidth(), levelY + y * getBlockImage().getHeight(), scoreValue > 1);
				}
			}
		}

		// Go through the lines from the top to the bottom to remove complete
		// lines
		for (int y = LEVEL_HEIGHT_BLOCKS - 1; y >= 0; y--) {
			boolean lineComplete;
			do {
				// Check if the line is complete
				lineComplete = true;
				for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
					if (getBlock(x, y) == null) {
						lineComplete = false;
						break;
					}
				}

				if (lineComplete) {
					for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
						// Move the blocks to the pool
						blockPool.put(getBlock(x, y));
						setBlock(x, y, null);
					}

					// And move all blocks above this one downwards
					for (int y2 = y; y2 > 0; y2--) {
						for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
							moveBlock(x, y2 - 1, x, y2);
						}
					}
				}

			} while (lineComplete);
		}

		if (playBonusSound) {
			bonusSound.play(1f, SquareSettings.soundVolume);
		} else if (playCompleteSound) {
			completeSound.play(1f, SquareSettings.soundVolume);
		}
	}

	/**
	 * Move a block within the level.
	 * 
	 * @param xFrom
	 * @param yFrom
	 * @param xTo
	 * @param yTo
	 */
	private void moveBlock(int xFrom, int yFrom, int xTo, int yTo) {
		setBlock(xTo, yTo, getBlock(xFrom, yFrom));
		setBlock(xFrom, yFrom, null);
	}

	/**
	 * Set the preview tetromino as the current tetromino, and generate a new random preview tetromino.
	 * 
	 * @throws SlickException
	 */
	private void nextTetromino() throws SlickException {
		// Choose a starting point outside of the screen
		currentTetromino.setup(tetrominoPreview.getShape(), LEVEL_WIDTH_BLOCKS / 2, -6,
				tetrominoPreview.getColorIndex());

		int colorIndex = random.nextInt(BLOCK_COLORS.length);
		tetrominoPreview.set(random.nextInt(Tetromino.SHAPE_COORDINATES.length), colorIndex);
	}

	/**
	 * Generate some random blocks for testing purposes.
	 * 
	 * @throws SlickException
	 */
	public void generateRandomLevel() throws SlickException {
		for (int x = 0; x < LEVEL_WIDTH_BLOCKS; x++) {
			for (int y = 0; y < LEVEL_HEIGHT_BLOCKS; y++) {
				levelBlocks[x][y] = blockPool.get(random.nextInt(BLOCK_COLORS.length));
			}
		}
	}
}
