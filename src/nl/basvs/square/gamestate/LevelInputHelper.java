package nl.basvs.square.gamestate;

import org.newdawn.slick.Input;

import nl.basvs.lib.InputHelper;

/**
 * Helper class for handling controls specific to Square.
 */
public class LevelInputHelper {

	private InputHelper input;

	public LevelInputHelper(InputHelper input) {
		this.input = input;
	}

	/**
	 * Was a control pressed for moving the tetromino left?
	 * 
	 * @return
	 */
	public boolean isMoveLeftControlDown() {
		return input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A);
	}

	/**
	 * Was a control pressed for moving the tetromino right?
	 * 
	 * @return
	 */
	public boolean isMoveRightControlDown() {
		return input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D);
	}

	/**
	 * Was a control pressed for rotating the tetromino clockwise?
	 * 
	 * @return
	 */
	public boolean isRotateClockwiseControlPressed() {
		return input.getKeyPressed() == Input.KEY_UP || input.getKeyPressed() == Input.KEY_W;
	}

	/**
	 * Was a control pressed for rotating the tetromino clockwise?
	 * 
	 * @return
	 */
	public boolean isRotateCounterClockwiseControlPressed() {
		return input.getKeyPressed() == Input.KEY_SPACE || input.getKeyPressed() == Input.KEY_Q;
	}

	/**
	 * Was a control pressed for moving the tetromino downwards?
	 * 
	 * @return
	 */
	public boolean isMoveDownControlDown() {
		return input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S);
	}
}
