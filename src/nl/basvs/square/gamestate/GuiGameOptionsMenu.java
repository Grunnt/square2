package nl.basvs.square.gamestate;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.SettingsHelper;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;
import nl.basvs.lib.gui.LayoutHelper;
import nl.basvs.lib.gui.LayoutHelper.HorizontalAlignment;
import nl.basvs.lib.gui.LayoutHelper.VerticalAlignment;
import nl.basvs.lib.gui.element.AbstractGuiElement;
import nl.basvs.lib.gui.element.GuiButton;
import nl.basvs.lib.gui.element.GuiCheckBox;
import nl.basvs.lib.gui.element.GuiPanel;
import nl.basvs.lib.gui.element.GuiSlider;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.Square2;
import nl.basvs.square.SquareSettings;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * A special panel that can be used to change game options.
 */
public class GuiGameOptionsMenu extends GuiPanel implements IGuiListener {

	private static final String ID_OPTIONS_MENU_SOUND_SLIDER = "optionsSound";
	private static final String ID_OPTIONS_MENU_FULLSCREEN_BUTTON = "optionsFullScreen";
	private static final String ID_OPTIONS_MENU_RESUME_BUTTON = "optionsResume";
	private static final String ID_OPTIONS_MENU_EXIT_BUTTON = "optionsExit";

	private Sound clickSound;

	// Gui elements that we need to access later
	private GuiSlider sliderSound;
	private GuiCheckBox fullScreenCheckBox;

	private boolean exitRequested;

	/**
	 * Get the volume that was set.
	 * 
	 * @return
	 */
	public float getSoundVolume() {
		return sliderSound.getValue();
	}

	/**
	 * Check if the full screen setting was selected.
	 * 
	 * @return
	 */
	public boolean isFullScreenSelected() {
		return fullScreenCheckBox.isChecked();
	}

	/**
	 * Check if the exit button was pressed.
	 * 
	 * @return
	 */
	public boolean isExitRequested() {
		return exitRequested;
	}

	/**
	 * Create a help menu.
	 * 
	 * @param id
	 * @param x
	 * @param y
	 * @param style
	 * @param input
	 * @throws SlickException
	 */
	public GuiGameOptionsMenu(String id, int x, int y, GuiStyle style, InputHelper input) throws SlickException {
		super(id, x - 175, y - 117, 450, 235, 8, style);

		// Setup some gui elements
		sliderSound = new GuiSlider(input, ID_OPTIONS_MENU_SOUND_SLIDER, "Sound", 0.75f, 1, 1, 400, 75, 8, style);
		sliderSound.addListener(this);
		addChild(sliderSound);

		fullScreenCheckBox = new GuiCheckBox(input, ID_OPTIONS_MENU_FULLSCREEN_BUTTON, "Full screen",
				(Boolean) SettingsHelper.getInstance().getSetting(Square2.SETTING_FULLSCREEN), 1, 1, 400, 50, 8, style);
		fullScreenCheckBox.addListener(this);
		addChild(fullScreenCheckBox);

		GuiButton optionsResumeButton = new GuiButton(input, ID_OPTIONS_MENU_RESUME_BUTTON, "Resume", 1, 1, 196, 50, 0,
				style);
		optionsResumeButton.addListener(this);
		addChild(optionsResumeButton);

		GuiButton optionsExitButton = new GuiButton(input, ID_OPTIONS_MENU_EXIT_BUTTON, "Main menu", 1, 1, 196, 50, 0,
				style);
		optionsExitButton.addListener(this);
		addChild(optionsExitButton);

		// Layout the panel
		LayoutHelper.layoutRows(this, HorizontalAlignment.Centered, 3, VerticalAlignment.Centered, 3, true, 1, 1, 2);

		// We start inactive
		setStatus(GuiStatus.Inactive);

		// Setup sound
		ResourceManager rm = ResourceManager.getInstance();
		clickSound = rm.getSound("audio/action");
	}

	@Override
	public void action(AbstractGuiElement origin) throws Exception {

		if (origin instanceof GuiButton) {

			// Make a button click sound
			clickSound.play(1f, sliderSound.getValue());

			// Handle button actions
			GuiButton button = (GuiButton) origin;
			if (button.getId().equals(ID_OPTIONS_MENU_RESUME_BUTTON)
					|| origin.getId().equals(ID_OPTIONS_MENU_EXIT_BUTTON)) {
				// The options menu is being exited, so we save the new settings
				boolean fullScreenSetting = fullScreenCheckBox.isChecked();
				SettingsHelper settings = SettingsHelper.getInstance();
				settings.putSetting(Square2.SETTING_SOUND_VOLUME, sliderSound.getValue());
				settings.putSetting(Square2.SETTING_FULLSCREEN, fullScreenSetting);
				settings.save();

				SquareSettings.soundVolume = (Float) SettingsHelper.getInstance().getSetting(
						Square2.SETTING_SOUND_VOLUME);
				SquareSettings.fullScreen = (Boolean) SettingsHelper.getInstance().getSetting(
						Square2.SETTING_FULLSCREEN);

				exitRequested = false;

				if (origin.getId().equals(ID_OPTIONS_MENU_EXIT_BUTTON)) {

					exitRequested = true;

				}

				// Signal the listeners that "Resume" or "Exit" was pressed
				for (IGuiListener listener : listeners) {
					listener.action(this);
				}
			}
		} else if (origin instanceof GuiCheckBox) {
			// Make a button click sound
			clickSound.play(1f, SquareSettings.soundVolume);
		}
	}

	@Override
	protected void doUpdate(int delta) throws Exception {

		// Did the user stop dragging the sound slider?
		if (sliderSound.isStoppedDragging()) {
			clickSound.play(1f, sliderSound.getValue());
		}

		super.doUpdate(delta);
	}

	@Override
	public void preActivation() {
		// Retrieve the sound volume setting
		float soundVolume = (Float) SettingsHelper.getInstance().getSetting(Square2.SETTING_SOUND_VOLUME);
		sliderSound.setValue(soundVolume);
		fullScreenCheckBox.setChecked((Boolean) SettingsHelper.getInstance().getSetting(Square2.SETTING_FULLSCREEN));
	}
}