package nl.basvs.square.gamestate;

import org.newdawn.slick.SlickException;

/**
 * Simple visual preview for the next tetromino the player will get.
 */
public class TetrominoPreview {

	// Information about the next tetromino
	private int x, y;
	private int shape;
	private int colorIndex;

	// Level, for rendering the preview
	private Level level;

	public int getShape() {
		return shape;
	}

	public int getColorIndex() {
		return colorIndex;
	}

	/**
	 * Create a preview for tetromino's.
	 * 
	 * @param x
	 * @param y
	 * @throws SlickException
	 */
	public TetrominoPreview(int x, int y, Level level) throws SlickException {
		this.x = x;
		this.y = y;
		this.level = level;
	}

	/**
	 * Set the shape of the tetromino to preview.
	 * 
	 * @param shape
	 * @param color
	 */
	public void set(int shape, int colorIndex) {
		this.shape = shape;
		this.colorIndex = colorIndex;
	}

	/**
	 * Render the tetromino preview.
	 */
	public void render() {

		int width = level.getBlockImage().getWidth() * 3;
		int height = level.getBlockImage().getHeight() * 2;

		if (shape == 3) {
			width = level.getBlockImage().getWidth() * 4;
			height = level.getBlockImage().getHeight();
		} else if (shape == 6) {
			width = level.getBlockImage().getWidth() * 2;
			height = level.getBlockImage().getHeight() * 2;
		}

		for (int b = 0; b < 4; b++) {
			int bX = Tetromino.SHAPE_COORDINATES[shape][b][0];
			int bY = Tetromino.SHAPE_COORDINATES[shape][b][1];

			level.getBlockImage().draw(x + bX * level.getBlockImage().getWidth() - width / 2,
					y + bY * level.getBlockImage().getHeight() - height / 2, Level.BLOCK_COLORS[colorIndex]);
		}
	}
}
