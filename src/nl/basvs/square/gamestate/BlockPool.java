package nl.basvs.square.gamestate;

import java.util.Stack;

import org.newdawn.slick.SlickException;

/**
 * A simple object pool that we use to avoid garbage collection in the main game loop.
 */
public class BlockPool {

	// A stack is handy (and fast) for a pool.
	private Stack<Block> pool = new Stack<Block>();

	/**
	 * Get a block from the pool, or a new block if the pool is empty. The block's parameters are overwritten with the
	 * parameters that are passed.
	 * 
	 * @param colorIndex
	 * @return
	 * @throws SlickException
	 */
	public Block get(int colorIndex) throws SlickException {
		Block result = null;
		if (pool.isEmpty()) {
			result = new Block();
		} else {
			result = pool.pop();
		}
		// Overwrite the block's parameters with the ones we received.
		result.reset(colorIndex);
		return result;
	}

	/**
	 * Return a block to the pool.
	 * 
	 * @param block
	 */
	public void put(Block block) {
		pool.push(block);
	}
}