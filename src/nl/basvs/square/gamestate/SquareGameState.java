package nl.basvs.square.gamestate;

import nl.basvs.lib.InputHelper;
import nl.basvs.lib.gamestate.AbstractGame;
import nl.basvs.lib.gamestate.AbstractGameState;
import nl.basvs.lib.graphics.Overlay;
import nl.basvs.lib.gui.GuiCursor;
import nl.basvs.lib.gui.GuiManager;
import nl.basvs.lib.gui.GuiStyle;
import nl.basvs.lib.gui.IGuiListener;
import nl.basvs.lib.gui.LayoutHelper;
import nl.basvs.lib.gui.LayoutHelper.HorizontalAlignment;
import nl.basvs.lib.gui.LayoutHelper.VerticalAlignment;
import nl.basvs.lib.gui.element.AbstractGuiElement;
import nl.basvs.lib.gui.element.AbstractGuiElement.GuiStatus;
import nl.basvs.lib.gui.element.GuiButton;
import nl.basvs.lib.gui.element.GuiPanel;
import nl.basvs.lib.gui.element.GuiTextField;
import nl.basvs.lib.gui.transition.GuiRotateTransition;
import nl.basvs.lib.resource.ResourceManager;
import nl.basvs.square.Square2;
import nl.basvs.square.SquareBackground;
import nl.basvs.square.SquareSettings;
import nl.basvs.square.menustate.HighScoreEntry;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

/**
 * Square main game state.
 */
public class SquareGameState extends AbstractGameState implements IGuiListener {

	// Substates for the game state
	public enum SubStates {
		InGame, Options, GameOver, HighScore
	};

	private SubStates subState;

	// The game level, which is basically the actual game
	private Level level;

	// Center of the display
	private int centerX;
	private int centerY;

	// A small "Square 2" title in the corner of the screen
	private Image smallTitleImage;
	private Image grunntImage;
	private Image fieldImage;
	private Image scoreImage;
	private Image highestImage;
	private Image nextImage;

	// Color of the playing field borders
	private Color playFieldColor = new Color(33, 46, 125, 200);

	// The font for drawing the score
	private UnicodeFont font;
	private String highScoreString;

	// Options substate data
	private static final String ID_OPTIONS_MENU_CLOSED = "optionsClosed";
	private GuiManager guiManager;
	private GuiGameOptionsMenu optionsMenu;

	// Gameover substate data
	private static final float GAME_OVER_INTERVAL = 1000;
	private int gameOverDisplayPhase;
	private float gameOverProgress;
	private Image gameOver1Image;
	private Image gameOver2Image;

	// High score substate data
	private static final String ID_SCORE_OK_BUTTON = "scoreOk";
	private GuiTextField nameTextField;

	// Fancy moving background
	private SquareBackground background;

	// A black overlay for transitions
	private Overlay blackOverlay;

	/**
	 * Create the game state.
	 * 
	 * @param container
	 * @param game
	 * @param input
	 * @throws Exception
	 */
	public SquareGameState(GameContainer container, AbstractGame game, InputHelper input) throws Exception {
		super(container, game, input);

		centerX = container.getWidth() / 2;
		centerY = container.getHeight() / 2;

		ResourceManager rm = ResourceManager.getInstance();
		smallTitleImage = rm.getImage("logosmall");
		grunntImage = rm.getImage("basvs");
		fieldImage = rm.getImage("game/playfield");
		scoreImage = rm.getImage("score");
		highestImage = rm.getImage("highest");
		nextImage = rm.getImage("next");
		gameOver1Image = rm.getImage("gameover1");
		gameOver2Image = rm.getImage("gameover2");

		guiManager = new GuiManager(input);
		guiManager.setCursor(new GuiCursor(rm.getImage("gui/cursor"), 5, 1));

		blackOverlay = new Overlay(Color.black, container);

		font = rm.getFont("gui/textfont");

		level = new Level(input, 313, 59);
		ScoreCounter counter = new ScoreCounter(level, 150, 240);
		level.setScoreCounter(counter);
		TetrominoPreview tetrominoPreview = new TetrominoPreview(150, 530, level);
		level.setTetrominoPreview(tetrominoPreview);

		buildOptionsMenu();

		// Setup a fancy moving background
		background = new SquareBackground(container.getWidth(), container.getHeight());
	}

	/**
	 * Reset the game state, and setup a level for a certain level size.
	 * 
	 * @param size
	 * @throws SlickException
	 */
	public void setup() throws SlickException {
		level.setup();
		subState = SubStates.InGame;

		guiManager.setCursorVisible(false);

		highScoreString = Integer.toString(Square2.getScores().getScores().get(0).getScore());
		// level.generateRandomLevel();
	}

	/**
	 * Setup the in-game options menu.
	 * 
	 * @throws SlickException
	 */
	private void buildOptionsMenu() throws SlickException {

		GuiStyle style = Square2.getStyle();

		optionsMenu = new GuiGameOptionsMenu(ID_OPTIONS_MENU_CLOSED, centerX, centerY, style, input);
		optionsMenu.addListener(this);
		optionsMenu.setTransitionEffect(new GuiRotateTransition(container, 2));
		guiManager.addPanel("options", optionsMenu);

		GuiPanel scorePanel = new GuiPanel("highscore", centerX - 400, centerY, 780, 130, 8, style);
		scorePanel.setTransitionEffect(new GuiRotateTransition(container, 2));
		scorePanel.setStatus(GuiStatus.Inactive);
		nameTextField = new GuiTextField(input, "nameText", "Highscore! Please enter your name: ", 0, 0, 750, 50, 8,
				style);
		scorePanel.addChild(nameTextField);
		GuiButton okButton = new GuiButton(input, ID_SCORE_OK_BUTTON, "Ok!", 0, 0, 80, 50, 8, style);
		okButton.addListener(this);
		scorePanel.addChild(okButton);
		LayoutHelper.layoutRows(scorePanel, HorizontalAlignment.Centered, 3, VerticalAlignment.Centered, 3, true);
		guiManager.addPanel(scorePanel.getId(), scorePanel);
	}

	@Override
	protected void doRender() throws Exception {
		// Render the moving background
		background.render();

		// Draw the title
		smallTitleImage.draw(30, 30);
		grunntImage
				.draw(container.getWidth() - grunntImage.getWidth(), container.getHeight() - grunntImage.getHeight());

		// Draw headers for the left bar
		scoreImage.draw(150 - scoreImage.getWidth() / 2, 150);
		highestImage.draw(150 - highestImage.getWidth() / 2, 300);
		nextImage.draw(150 - nextImage.getWidth() / 2, 450);

		// Draw the previous high score for this level size
		int strWidth = font.getWidth(highScoreString);
		font.drawString(150 - strWidth / 2, 390 - font.getLineHeight() / 2, highScoreString);

		// Draw the playing field
		fieldImage.draw(299, 0, playFieldColor);

		// Draw the level in the center of the screen by using a translate
		level.render();

		// Do some special rendering for the different substates
		switch (subState) {
		case InGame:
			break;
		case Options:
			blackOverlay.draw(0.35f);
			break;
		case GameOver:
			blackOverlay.draw(0.35f);
			if (gameOverDisplayPhase == 0) {
				gameOver1Image.draw(centerX - ((1f - gameOverProgress) * centerX * 1.5f) - gameOver1Image.getWidth()
						/ 2, centerY - 200);
			} else if (gameOverDisplayPhase == 1) {
				gameOver1Image.draw(centerX - gameOver1Image.getWidth() / 2, centerY - 200);
				gameOver2Image.draw(centerX + ((1f - gameOverProgress) * centerX * 1.5f) - gameOver2Image.getWidth()
						/ 2, centerY);
			} else {
				gameOver1Image.draw(centerX - gameOver1Image.getWidth() / 2, centerY - 200);
				gameOver2Image.draw(centerX - gameOver2Image.getWidth() / 2, centerY);
			}

			break;
		case HighScore:
			blackOverlay.draw(0.35f);
			break;
		}

		guiManager.render();

		if (isTransitioning()) {
			blackOverlay.draw(1f - getTransitionFactor());
		}
	}

	@Override
	protected void doUpdate(int delta) throws Exception {

		// Update the moving background
		background.update(delta);

		guiManager.update(delta);

		// Handle each substate differently
		switch (subState) {
		case InGame:
			if (input.getKeyPressed() == Input.KEY_PAUSE || input.getKeyPressed() == Input.KEY_P
					|| input.getKeyPressed() == Input.KEY_ESCAPE) {
				subState = SubStates.Options;
				guiManager.setCursorVisible(true);
				guiManager.activate("options", true);
			}
			level.update(delta);
			if (level.isGameOver()) {
				gameOverProgress = 0f;
				gameOverDisplayPhase = 0;
				subState = SubStates.GameOver;
			}
			break;
		case Options:
			if (input.getKeyPressed() == Input.KEY_PAUSE || input.getKeyPressed() == Input.KEY_P
					|| input.getKeyPressed() == Input.KEY_ESCAPE) {
				subState = SubStates.InGame;
				guiManager.setCursorVisible(false);
				guiManager.deactivate("options");
			}
			break;
		case GameOver:
			level.update(delta);
			gameOverProgress += (float) delta / GAME_OVER_INTERVAL;
			if (gameOverProgress >= 1f && level.getScoreCounter().isFinished()) {
				gameOverDisplayPhase++;

				if (gameOverDisplayPhase >= 3) {
					// Final phase: we switch to another substate
					if (Square2.getScores().isHighScore(level.getScoreCounter().getScore())) {
						// Its a high score! So we show a popup to the user to enter a name
						subState = SubStates.HighScore;
						guiManager.setCursorVisible(true);
						guiManager.activate("highscore", true);
						nameTextField.setFocus(true);
					} else { // We waited for some time and the score points are all collected, so its safe to exit
						game.activate(Square2.STATE_MAIN_MENU, true);
					}
				} else
					gameOverProgress = 0f;
			}
			break;
		}
	}

	@Override
	protected void activating() {
		// Do nothing
	}

	@Override
	protected void deactivating() {
		// Do nothing
	}

	@Override
	protected void activated() throws Exception {
		// Do nothing
	}

	@Override
	protected void deactivated() throws Exception {
		// Do nothing
	}

	@Override
	public void action(AbstractGuiElement origin) throws Exception {
		// A button was pressed
		if (origin.getId().equals(ID_OPTIONS_MENU_CLOSED)) {
			// The game should resume
			SquareSettings.soundVolume = optionsMenu.getSoundVolume();
			boolean fullScreenSetting = optionsMenu.isFullScreenSelected();

			// Change fullscreen setting if required
			if (container.isFullscreen() != fullScreenSetting) {
				container.setFullscreen(fullScreenSetting);
			}

			if (optionsMenu.isExitRequested()) {

				// Exit to main menu
				guiManager.getPanel("options").setStatus(GuiStatus.Inactive);
				game.activate(Square2.STATE_MAIN_MENU, true);

			} else {

				guiManager.deactivate("options");
			}

			subState = SubStates.InGame;
			guiManager.setCursorVisible(false);

		} else if (origin.getId().equals(ID_SCORE_OK_BUTTON)) {

			// The high score name entry "ok" button was clicked
			if (!nameTextField.getText().trim().equals("")) {

				// Insert the score to the high score lists
				Square2.getScores()

				.insertHighScore(new HighScoreEntry(nameTextField.getText(), level.getScoreCounter().getScore()));
				Square2.getScores().save();

				// Return to main menu
				guiManager.getPanel("highscore").setStatus(GuiStatus.Inactive);
				nameTextField.setFocus(false);
				game.activate(Square2.STATE_MAIN_MENU, true);
			} else {
				// We ignore OK clicks as long as nothing was entered
			}
		}
	}
}
