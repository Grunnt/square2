package nl.basvs.square.gamestate;

import java.util.ArrayList;
import java.util.List;

import nl.basvs.lib.resource.ResourceManager;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;

/**
 * A score counter and visual score points helper class. It maintains the current score, and new score points may be
 * added using a visual effect showing the point moving from the source (e.g. a block of a completed line) towards the
 * score counter. Upon arrival, the point will be added to the score total.
 */
public class ScoreCounter {

	// Score counter location
	private int x, y;
	private int score;
	// Font to render the score counter in
	private UnicodeFont font;

	// Pool of score and bonus points
	private List<ScorePoint> points = new ArrayList<ScorePoint>();

	// The level, for adding points to the score and accessing the block image
	private Level level;

	/**
	 * Get total score in this counter.
	 * 
	 * @return
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Create a new score counter at a specific location.
	 * 
	 * @param x
	 * @param y
	 * @throws SlickException
	 */
	public ScoreCounter(Level level, int x, int y) throws SlickException {
		this.level = level;
		this.x = x;
		this.y = y;
		font = ResourceManager.getInstance().getFont("gui/textfont");
		for (int i = 0; i < 50; i++) {
			points.add(new ScorePoint(level));
		}
		score = 12345;
	}

	/**
	 * Render the score counter and all active score points.
	 */
	public void render() {
		for (ScorePoint sp : points) {
			sp.render();
		}
		String scoreString = Integer.toString(score);
		int scoreWidth = font.getWidth(scoreString);
		int scoreHeight = font.getLineHeight();
		font.drawString(x - scoreWidth / 2, y - scoreHeight / 2, scoreString);
	}

	/**
	 * Update the score points locations, and check for any arrivals that should be added to the total score.
	 * 
	 * @param delta
	 */
	public void update(int delta) {
		for (ScorePoint sp : points) {
			sp.update(delta);
		}
		checkForArrivals();
	}

	/**
	 * Create a new point at a specific location.
	 * 
	 * @param value
	 * @param sourceX
	 * @param sourceY
	 * @throws SlickException
	 */
	public void addPoint(int value, int colorIndex, int sourceX, int sourceY, boolean flash) throws SlickException {
		// Use one of the objects in the pool
		for (ScorePoint point : points) {
			if (!point.isActive()) {
				point.activate(value, colorIndex, sourceX, sourceY, x, y, flash);
				return;
			}
		}
		// If none are available, we add a new one
		ScorePoint newPoint = new ScorePoint(level);
		newPoint.activate(value, colorIndex, sourceX, sourceY, x, y, flash);
		points.add(newPoint);
	}

	/**
	 * Check if any score point has arrived at the score counter. If so, it will be deactivated and its value added to
	 * the counter.
	 */
	private void checkForArrivals() {
		for (ScorePoint sp : points) {
			if (sp.isActive() && sp.isArrived()) {
				score += sp.getValue();
				sp.deactivate();
			}
		}
	}

	/**
	 * Reset the score counter and deactivate all score points.
	 */
	public void reset() {
		for (ScorePoint point : points) {
			point.deactivate();
		}
		score = 0;
	}

	/**
	 * Check if all points have arrived and added to the score counter, so that we don't miss out on points.
	 * 
	 * @return
	 */
	public boolean isFinished() {
		for (ScorePoint point : points) {
			if (point.isActive() && !point.isArrived())
				return false;
		}
		return true;
	}
}
