package nl.basvs.square.gamestate;

/**
 * Basic unit of the Square game: a (square) block.
 */
public class Block {

	// Color of this block
	private int colorIndex;
	// Time until flashing stops
	private int flashTimeRemaining;

	public int getColorIndex() {
		return colorIndex;
	}

	public int getFlashTimeRemaining() {
		return flashTimeRemaining;
	}

	public void setFlashTimeRemaining(int flashTimeRemaining) {
		this.flashTimeRemaining = flashTimeRemaining;
	}

	/**
	 * Reset the block.
	 * 
	 * @param colorIndex
	 */
	public void reset(int colorIndex) {
		this.colorIndex = colorIndex;
		flashTimeRemaining = 0;
	}
}