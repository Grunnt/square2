package nl.basvs.square.gamestate;

import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;

/**
 * A visual score point object, which moves from the source of the score (i.e. a block of a completed line) to the score
 * counter, where it will be added to the score upon arrival.
 */
public class ScorePoint {

	// The time in ms it takes for this point to move from its origin to the score counter.
	private static final float MOVEMENT_DURATION_MS = 1000;
	private static final int FLASH_DURATION_MS = 20;

	// To avoid garbage collection, we maintain a list of score point objects, and only draw/update them if they are
	// active.
	private boolean active;

	// The point is moving from an origin to a destination, and is progressing along a curve
	private float sourceX, sourceY;
	private float progress;
	private int targetX, targetY;

	// The value of a point gets added to the score counter
	private int value;
	private boolean flash;

	// The point has the same color as the block that generated the score.
	private Color color = new Color(1f, 1f, 1f);

	// Points with a high value appear to be flashing
	private Color flashColor = Color.white;
	private boolean flashOn;
	private int flashInterval;

	private Level level;

	/**
	 * Check if this point is active.
	 * 
	 * @return
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Get the score value that this point has.
	 * 
	 * @return
	 */
	public int getValue() {
		return value;
	}

	public boolean isFlash() {
		return flash;
	}

	public void setFlash(boolean flash) {
		this.flash = flash;
	}

	/**
	 * Create a new point/
	 * 
	 * @throws SlickException
	 */
	public ScorePoint(Level level) throws SlickException {
		this.level = level;
		active = false;
	}

	/**
	 * Check whether this point's movement has completed.
	 * 
	 * @return
	 */
	public boolean isArrived() {
		return progress >= 1f;
	}

	/**
	 * Deactivate this point.
	 */
	public void deactivate() {
		this.active = false;
	}

	/**
	 * Activate this point.
	 * 
	 * @param value
	 * @param colorIndex
	 * @param sourceX
	 * @param sourceY
	 * @param targetX
	 * @param targetY
	 */
	public void activate(int value, int colorIndex, int sourceX, int sourceY, int targetX, int targetY, boolean flash) {
		this.value = value;
		this.sourceX = sourceX;
		this.sourceY = sourceY;
		this.targetX = targetX;
		this.targetY = targetY;
		this.color.a = 1f;
		this.color.r = level.BLOCK_COLORS_TRANSPARENT[colorIndex].r;
		this.color.g = level.BLOCK_COLORS_TRANSPARENT[colorIndex].g;
		this.color.b = level.BLOCK_COLORS_TRANSPARENT[colorIndex].b;
		progress = 0f;
		this.active = true;
		flashInterval = FLASH_DURATION_MS;
		flashOn = false;
	}

	/**
	 * Update the point's position.
	 * 
	 * @param delta
	 */
	public void update(int delta) {
		if (active) {
			progress += ((float) delta / MOVEMENT_DURATION_MS);
			if (progress > 1f)
				progress = 1f;
			color.a = 0.75f - (progress / 2f);

			// If this is a high value point, keep flashing
			if (flash) {
				flashInterval -= delta;
				if (flashInterval <= 0) {
					flashOn = !flashOn;
					flashInterval = FLASH_DURATION_MS;
				}

			}
		}
	}

	/**
	 * Draw the score point at a position derived from its movement progress.
	 */
	public void render() {
		if (active) {

			// We use a simple formula to get a curve movement
			int pointX = (int) (sourceX + progress * (targetX - sourceX));
			int pointY = (int) (sourceY + (progress * progress) * (targetY - sourceY));

			// Draw a different color in case flash is on
			if (flashOn) {
				level.getBlockImage().draw(pointX - level.getBlockImage().getWidth() / 2,
						pointY - level.getBlockImage().getWidth() / 2, 1f - progress / 2f, flashColor);
			} else {
				level.getBlockImage().draw(pointX - level.getBlockImage().getWidth() / 2,
						pointY - level.getBlockImage().getWidth() / 2, 1f - progress / 2f, color);
			}
		}
	}
}
