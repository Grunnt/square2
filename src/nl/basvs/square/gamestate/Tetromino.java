package nl.basvs.square.gamestate;

import nl.basvs.square.SquareSettings;

import org.newdawn.slick.SlickException;

/**
 * A tetromino, which is a shape consisting of four blocks. The tetromino is controlled by the player. If the tetromino
 * is placed in the level, it stops being a tetromino and becomes part of the level.
 */
public class Tetromino {

	public static final float FALL_FASTER_FACTOR = 5f;
	private static final float XMOVE_MAX_INTERVAL = 350;
	private static final float XMOVE_MIN_INTERVAL = 35;
	private static final float XMOVE_INTERVAL_REDUCEE_PER_MS = 1f;
	private static final int BLOCK_FLASH_TIME = 100;

	// The 7 possible shapes using four blocks
	// We express these shapes in relative coordinates [shape][block][x/y]
	public static final int[][][] SHAPE_COORDINATES = { { { 0, 1 }, { 1, 1 }, { 2, 1 }, { 1, 2 } },
			{ { 0, 2 }, { 1, 1 }, { 2, 1 }, { 1, 2 } }, { { 0, 1 }, { 1, 1 }, { 1, 2 }, { 2, 2 } },
			{ { 0, 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 } }, { { 0, 1 }, { 1, 1 }, { 2, 1 }, { 0, 2 } },
			{ { 0, 1 }, { 1, 1 }, { 2, 1 }, { 2, 2 } }, { { 0, 1 }, { 1, 1 }, { 0, 2 }, { 1, 2 } } };

	// Information about the tetromino
	private int x, y;
	private int shape;
	private int colorIndex;
	private int rotation;

	// The level, so that we can check for collisions and for the borders
	private Level level;

	// Falling
	private float fallProgress;
	private boolean fallBlocked;

	// Horizontal movement
	private float horizontalInterval;
	private float horizontalProgress;

	// Game over if we could not place the last tetromino inside the level boundaries
	private boolean gameOver;

	/**
	 * Check if this block's fall was blocked during the last update.
	 * 
	 * @return
	 */
	public boolean isFallBlocked() {
		return fallBlocked;
	}

	/**
	 * Get X of the tetromino (which is the position of the 1,1 block).
	 * 
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get Y of the tetromino (which is the position of the 1,1 block).
	 * 
	 * @return
	 */
	public int getY() {
		return y;
	}

	/**
	 * Get the color of this tetromino.
	 * 
	 * @return
	 */
	public int getColorIndex() {
		return colorIndex;
	}

	/**
	 * Create a new tetromino holder.
	 * 
	 * @param level
	 */
	public Tetromino(Level level) {
		this.level = level;
	}

	/**
	 * Replace this tetromino with a new one.
	 * 
	 * @param shape
	 * @param x
	 * @param y
	 * @param color
	 * @throws SlickException
	 */
	public void setup(int shape, int x, int y, int colorIndex) throws SlickException {
		this.shape = shape;
		this.x = x;
		this.y = y;
		this.colorIndex = colorIndex;
		this.rotation = 0;
		this.fallProgress = 0f;
		this.fallBlocked = false;
		this.horizontalInterval = XMOVE_MAX_INTERVAL;
		this.horizontalProgress = 1f;
		this.gameOver = false;
	}

	/**
	 * Get the rotated X coordinate of a block.
	 * 
	 * @param block
	 * @return
	 */
	public int getBlockX(int block) {
		int rX = SHAPE_COORDINATES[shape][block][0] - 1;
		int rY = SHAPE_COORDINATES[shape][block][1] - 1;
		int returnX = rX;
		if (shape == 3) {
			// The long bar will just toggle between horizontal and vertical (it does not have a neat rotation center
			// like the rest of the shapes).
			switch (rotation) {
			case 1:
				returnX = rY;
				break;
			case 3:
				returnX = rY;
				break;
			}
		} else if (shape < 6) {
			// Other shapes are rotated normally (except for the square one, which does not need rotating at all)
			switch (rotation) {
			case 1:
				returnX = -rY;
				break;
			case 2:
				returnX = -rX;
				break;
			case 3:
				returnX = rY;
				break;
			}
		}
		// Return the relative x + the tetromino x
		return returnX + x;
	}

	/**
	 * Get the rotated Y coordinate of a block.
	 * 
	 * @param block
	 * @return
	 */
	public int getBlockY(int block) {
		int rX = SHAPE_COORDINATES[shape][block][0] - 1;
		int rY = SHAPE_COORDINATES[shape][block][1] - 1;
		int returnY = rY;
		if (shape == 3) {
			// The long bar will just toggle between horizontal and vertical (it does not have a neat rotation center
			// like the rest of the shapes).
			switch (rotation) {
			case 1:
				returnY = rX;
				break;
			case 3:
				returnY = rX;
				break;
			}
		} else if (shape < 6) {
			// Other shapes are rotated normally (except for the square one, which does not need rotating at all)
			switch (rotation) {
			case 1:
				returnY = rX;
				break;
			case 2:
				returnY = -rY;
				break;
			case 3:
				returnY = -rX;
				break;
			}
		}
		// Return the relative y + the tetromino y
		return returnY + y;
	}

	/**
	 * Render the tetromino.
	 */
	public void render() {
		// Do not draw the falling progress if a block is on top of another block
		float drawProgress = fallProgress;
		for (int b = 0; b < 4; b++) {
			// Only check when inside the level
			if (getBlockY(b) >= -1) {
				if (getBlockY(b) + 1 >= Level.LEVEL_HEIGHT_BLOCKS
						|| level.getBlock(getBlockX(b), getBlockY(b) + 1) != null) {
					drawProgress = 0f;
				}
			}
		}

		// Draw all four blocks
		for (int b = 0; b < 4; b++) {
			level.getBlockImage().draw(level.getLevelX() + getBlockX(b) * level.getBlockImage().getWidth(),
					level.getLevelY() + (getBlockY(b) + drawProgress) * level.getBlockImage().getHeight(),
					Level.BLOCK_COLORS[colorIndex]);
		}
	}

	/**
	 * Update this tetromino.
	 * 
	 * @param delta
	 * @throws SlickException
	 */
	public void update(int delta) throws SlickException {

		// Handle user controlled rotation
		if (level.getInput().isRotateClockwiseControlPressed()) {
			rotation = (rotation + 1) % 4;
			if (isInCollision()) {
				// If we're in collision, we need to rotate back
				rotation = (rotation + 3) % 4;
			} else {
				if (shape != 6)
					level.getRotateSound().play(1f, SquareSettings.soundVolume);
			}
		}
		if (level.getInput().isRotateCounterClockwiseControlPressed()) {
			rotation = (rotation + 3) % 4;
			// If we're in collision, we need to rotate back
			if (isInCollision()) {
				rotation = (rotation + 1) % 4;
			} else {
				if (shape != 6)
					level.getRotateSound().play(1f, SquareSettings.soundVolume);
			}
		}

		// Handle horizontal movement
		if (level.getInput().isMoveLeftControlDown() || level.getInput().isMoveRightControlDown()) {
			if (handleHorizontalSpeed(delta)) {
				if (level.getInput().isMoveLeftControlDown()) {
					x--;
					if (isInCollision()) {
						x++;
					}
				}
				if (level.getInput().isMoveRightControlDown()) {
					x++;
					if (isInCollision()) {
						x--;
					}
				}
			}
		} else {
			horizontalInterval = XMOVE_MAX_INTERVAL;
			horizontalProgress = 1f;
		}

		// Handle downwards movement
		if (level.getInput().isMoveDownControlDown()) {
			fallProgress += (float) delta / (level.getCurrentFallInterval() / FALL_FASTER_FACTOR);
		} else {
			fallProgress += (float) delta / level.getCurrentFallInterval();
		}
		if (fallProgress >= 1f) {

			y++;
			if (isInCollision()) {
				y--;

				// Is any block still outside of the level?
				for (int b = 0; b < 4; b++) {
					if (getBlockY(b) < 0) {
						// It's game over!
						gameOver = true;
					}
				}

				if (!gameOver) {
					// We hit another block or the level bottom, so we got to place the block in the level.
					placeInLevel();
				}

				fallBlocked = true;
			} else {
				fallBlocked = false;
			}

			fallProgress = 0f;
		}
	}

	/**
	 * Update horizontal movement speed counters.
	 * 
	 * @return true if the tetromino may move horizontally one block
	 */
	private boolean handleHorizontalSpeed(int delta) {
		boolean move = false;

		horizontalProgress += (float) delta / horizontalInterval;

		if (horizontalProgress >= 1f) {
			move = true;
			horizontalProgress = 0f;
		}

		horizontalInterval -= XMOVE_INTERVAL_REDUCEE_PER_MS * delta;

		if (horizontalInterval < XMOVE_MIN_INTERVAL) {
			horizontalInterval = XMOVE_MIN_INTERVAL;
		}

		return move;
	}

	/**
	 * Place this tetromino in the level.
	 * 
	 * @throws SlickException
	 */
	private void placeInLevel() throws SlickException {
		for (int b = 0; b < 4; b++) {
			level.setBlock(getBlockX(b), getBlockY(b), level.getBlockPool().get(colorIndex));
			// Flash when the block is placed
			level.getBlock(getBlockX(b), getBlockY(b)).setFlashTimeRemaining(BLOCK_FLASH_TIME);
		}
	}

	/**
	 * Check if this tetromino is in collision with level blocks or the level border.
	 * 
	 * @param level
	 * @return
	 */
	private boolean isInCollision() {

		// Check all 4 blocks of the tetromino
		for (int b = 0; b < 4; b++) {

			// Is this block outside the level horizontally?
			if (getBlockX(b) < 0 || getBlockX(b) >= Level.LEVEL_WIDTH_BLOCKS) {
				return true;
			}

			// Is this block below the level bottom?
			if (getBlockY(b) >= Level.LEVEL_HEIGHT_BLOCKS) {
				return true;
			}

			// Is there a level block at this block's location?
			// First check: is the block inside the level?
			if (getBlockY(b) >= 0) {
				if (level.getBlock(getBlockX(b), getBlockY(b)) != null) {
					// There is a level block at this block's location, so its a collision
					return true;
				}
			}
		}

		// No collision found.
		return false;
	}
}
