package nl.basvs.square.gamestate;

public interface ILevelListener {

	public void gameOver(int score);

}
